#ifndef _DEMETRADETECTORRENOIRMESSENGER_HH
#define _DEMETRADETECTORRENOIRMESSENGER_HH

//---------------------------------------------------------------------------//

#include "globals.hh"
#include "G4UImessenger.hh"

//---------------------------------------------------------------------------//

class G4UIdirectory;
class G4UIcommand;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWithoutParameter;
class DEMETRADetectorRENOIR;

//---------------------------------------------------------------------------//

class DEMETRADetectorRENOIRMessenger : public G4UImessenger
{
public:
    
    //default constructor
    DEMETRADetectorRENOIRMessenger(DEMETRADetectorRENOIR *detector);
    
    //copy constructor
    //DEMETRADetectorRENOIRMessenger();
    
    //destructor
    ~DEMETRADetectorRENOIRMessenger();
    
    //public interface
    void SetNewValue(G4UIcommand *command, G4String newValues);
    
    //protected members
protected:
    
    //private  members
private:
    DEMETRADetectorRENOIR   *fDetectorPrimary;
    
    G4UIdirectory               *fDetectorDirectory;
    G4UIdirectory               *fShieldDirectory;

  //    G4UIcmdWithAString          *fRENOIRShieldingCmd;

  //    G4UIcmdWithAString          *fSourceCmd;
  //    G4UIcmdWithADoubleAndUnit   *fSourcePosCmd;

};
#endif
