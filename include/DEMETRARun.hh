#ifndef DEMETRARUN_HH
#define DEMETRARUN_HH

#include "G4Run.hh"

class G4Event;

class DEMETRARun : public G4Run 
{
public:
  DEMETRARun();
  virtual ~DEMETRARun() {};
  
  virtual void RecordEvent(const G4Event*);
  virtual void Merge(const G4Run*);
  G4double GetEmEnergy() const { return em_ene; }
  G4double GetHadEnergy() const { return had_ene; }
  G4double GetShowerShape() const { return shower_shape; }
private:
  G4double em_ene;
  G4double had_ene;
  G4double shower_shape;
  G4int DEMETRAID;
};
#endif
