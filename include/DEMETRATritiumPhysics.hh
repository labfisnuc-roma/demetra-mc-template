#ifndef DEMETRATritiumPhysics_h
#define DEMETRATritiumPhysics_h 1

//#include "G4VUserPhysicsList.hh"
#include "G4VPhysicsConstructor.hh"
#include "globals.hh"

//class DEMETRATritiumPhysics: public G4VUserPhysicsList
class DEMETRATritiumPhysics: public G4VPhysicsConstructor
{
public:
  /// constructor
  DEMETRATritiumPhysics();
  /// destructor
  virtual ~DEMETRATritiumPhysics();
  virtual void ConstructParticle();
  virtual void ConstructProcess();    
  virtual void ConstructDecay();
  /// Set user cuts
  //virtual void SetCuts();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

