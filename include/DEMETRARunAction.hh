#ifndef DEMETRARunAction_h
#define DEMETRARunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"

class G4Run;
class DEMETRARunActionMessenger;
class DEMETRADetectorConstruction;

class DEMETRARunAction : public G4UserRunAction
{
  public:
  DEMETRARunAction(DEMETRADetectorConstruction*);
  virtual ~DEMETRARunAction();
  
  virtual void BeginOfRunAction(const G4Run*);
  virtual void EndOfRunAction(const G4Run*);
  virtual G4Run* GenerateRun();
  
  void AddNumberOfMuonsDetector(){fNumberOfMuonsDetector++;};
  void SetOutFile(G4String fname) {FileName = fname;};
  void SetOutFileCut(G4int cut) {fOutFileCut = cut;};
  void SetRegisterOn(G4int regOn) {fRegisterOn = regOn;};
  void SetTotT(G4int cut) {fTotT = cut;};
  void SetHitsInfo(G4int cut) {fHitsInfo = cut;};

  private:
  
  DEMETRARunActionMessenger* fMessenger;  
  DEMETRADetectorConstruction* fDetector;
  int fNumberOfMuonsDetector;  
  G4String FileName;  
  G4int fOutFileCut;
  G4int fRegisterOn;
  G4int fTotT;
  G4int fHitsInfo;
};

#endif





