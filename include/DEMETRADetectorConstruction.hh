/* ************************************************
 * INFN RM1 GEANT4 SIMULATION GROUP 
 *
 * File:      DetectorConstruction.hh
 *
 **************************************************/

#pragma once

// STL //
#include <string>

// GEANT4 //
class G4VSolid;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Region;
class G4ProductionCuts;
class DEMETRADetectorConstructionMessenger;
class DEMETRASensitiveDetector;


#include "G4ThreeVector.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4RotationMatrix.hh"
#include "G4SystemOfUnits.hh"
#include "globals.hh"
#include <vector>


class DEMETRADetectorConstruction : public G4VUserDetectorConstruction
{
  public:

    DEMETRADetectorConstruction();
    ~DEMETRADetectorConstruction();

    G4VPhysicalVolume* Construct();
    void UpdateGeometry();

    void SetAirInnerRadius(G4double airinnerr) { airInnerRadius = airinnerr;}
    void SetAirThickness(G4double airthick) { airThickness = airthick;}
    void SetExternalRockThickness(G4double rockthick) {rockThicknessOuter = rockthick;}
    void SetProductionRockThickness(G4double rockthick) {productionLayerThickness = rockthick;}
    void SetInternalRockThickness(G4double rockthick) {rockThicknessInner = rockthick;}
    void SetShieldThick0(G4double st0) { thick0 = st0;}
    void SetShieldThick1(G4double st1) { thick1 = st1;}
    void SetShieldThick2(G4double st2) { thick2 = st2;}
    void SetShieldThick3(G4double st3) { thick3 = st3;}
    void SetShield0Material(G4String s0m) { Shield0MatStr = s0m;}
    void SetShield1Material(G4String s1m) { Shield1MatStr = s1m;}
    void SetShield2Material(G4String s2m) { Shield2MatStr = s2m;}
    void SetShield3Material(G4String s3m) { Shield3MatStr = s3m;}

    void SetDetX(G4double detx) { fDetX = detx;}
    void SetDetY(G4double dety) { fDetY = dety;}
    void SetDetZ(G4double detz) { fDetZ = detz;}

    void SetDEMETRALab(G4String lab) {DEMETRALab = lab;}
    G4String GetDEMETRALab() {return DEMETRALab;}
    void SetDEMETRAShielding(G4String shielding) {DEMETRAShielding = shielding;}
    G4String GetDEMETRAShielding() {return DEMETRAShielding;}
    void SetDEMETRASetup(G4String setup) {DEMETRASetup = setup;}
    G4String GetDEMETRASetup() {return DEMETRASetup;}

  // called by the analysis class; implememented in case we want to use an array of detectors
 
    void SetNumX(G4double numx) { NumX = numx;}
    G4int GetNumX() {return NumX;}
    void SetNumY(G4double numy) { NumY = numy;}
    G4int GetNumY() {return NumY;}
    void SetNumZ(G4double numz) { NumZ = numz;}
    G4int GetNumZ() {return NumZ;}
  
    G4ThreeVector GetTranslation()  {return tr_Tot;}  

  private:
    
    DEMETRADetectorConstructionMessenger* fMessenger;

    G4double NumX,NumY,NumZ;
 
    G4String DEMETRALab;
    G4String DEMETRAShielding;
    G4String DEMETRASetup;

    //G4VSolid * world_solid;
    //G4LogicalVolume* world_logical;
    //G4VPhysicalVolume* world_physical;
    
    G4double InsideVolume_OR;
    G4double InsideVolume_Z;
    G4double rockThicknessOuter;
    G4double rockThicknessInner;
    G4double productionLayerThickness;

    G4double airInnerRadius;
    G4double airThickness;
  
    G4double thick0;
    G4double thick1;
    G4double thick2;
    G4double thick3;

    G4double fDetX;
    G4double fDetY;
    G4double fDetZ;
 
    G4String Shield0MatStr;
    G4String Shield1MatStr;
    G4String Shield2MatStr;
    G4String Shield3MatStr;

   //Building blocks: logic volumes, sizes and positions
    G4ThreeVector  tr_Tot;
    G4LogicalVolume* Rock_log;
    G4ThreeVector size_Rock;
    G4ThreeVector tr_Rock;
    G4RotationMatrix rot_Rock;
    G4RotationMatrix absrot_Rock;
    G4LogicalVolume* Laboratory_log;
    G4ThreeVector size_Laboratory;
    G4ThreeVector tr_Laboratory;
    G4RotationMatrix rot_Laboratory;
    G4LogicalVolume* Shielding_log;
    G4ThreeVector size_Shielding;
    G4ThreeVector tr_Shielding;
    G4RotationMatrix rot_Shielding;
    G4RotationMatrix absrot_Shielding;
    G4LogicalVolume* InsideVolume_log;
    G4ThreeVector size_InsideVolume;
    G4ThreeVector tr_InsideVolume;
    G4RotationMatrix rot_InsideVolume;
    G4ThreeVector size_cad;
    G4ThreeVector tr_cad;
    G4RotationMatrix rot_cad;
    G4RotationMatrix absrot_cad;
    
    
    //CAD meshes
    G4VSolid * cad_shell_solid;
    G4VSolid * cad_camera_carter_solid;
    G4VSolid * cad_cameras_all_solid;
  
    
    // Logical volumes
    G4LogicalVolume* WorldVolume_log;
    G4LogicalVolume * cad_shell_logical;
    G4LogicalVolume * cad_camera_carter_logical;
    G4LogicalVolume * cad_cameras_all_logical;
    G4LogicalVolume* Detector_log;
  
    // Physical volumes
    G4VPhysicalVolume* WorldVolume_phys;
    G4VPhysicalVolume* AirBox_phys;
    G4VPhysicalVolume* Shield0_phys;
    G4VPhysicalVolume* Shield1_phys;
    G4VPhysicalVolume* Shield2_phys;
    G4VPhysicalVolume* Shield3_phys;
    G4VPhysicalVolume* Detector_phys;
    G4VPhysicalVolume* PbShield_phys;
    G4VPhysicalVolume* productionRockThinTube_phys;
    G4VPhysicalVolume * cad_shell_physical;
    G4VPhysicalVolume * cad_camera_carter_physical;
    G4VPhysicalVolume * cad_cameras_all_physical;

    // sensitive detector
    DEMETRASensitiveDetector* DEMETRASD;

    // regions for production cuts
    G4Region* fDetectorRegion;
    G4Region* fShieldRegion;

    G4ProductionCuts* fDetectorCuts;
    G4ProductionCuts* fShieldCuts;

};

