#ifndef DEMETRAPrimaryGeneratorActionMessenger_h
#define DEMETRAPrimaryGeneratorActionMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class DEMETRAPrimaryGeneratorAction;
class G4UIcmdWithAString;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class DEMETRAPrimaryGeneratorActionMessenger: public G4UImessenger
{
public:

  DEMETRAPrimaryGeneratorActionMessenger(DEMETRAPrimaryGeneratorAction*);
  ~DEMETRAPrimaryGeneratorActionMessenger();
    
  virtual void SetNewValue(G4UIcommand*, G4String);
    
private:
  DEMETRAPrimaryGeneratorAction* fDEMETRAPrimaryGeneratorAction;
  G4UIcmdWithAString* fDEMETRAGenTypeCmd;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
