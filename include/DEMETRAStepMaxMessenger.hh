#ifndef DEMETRAStepMaxMessenger_h
#define DEMETRAStepMaxMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class DEMETRAStepMax;
class G4UIcmdWithADoubleAndUnit;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class DEMETRAStepMaxMessenger: public G4UImessenger
{
public:

  DEMETRAStepMaxMessenger(DEMETRAStepMax*);
  ~DEMETRAStepMaxMessenger();
    
  virtual void SetNewValue(G4UIcommand*, G4String);
    
private:
  DEMETRAStepMax* fDEMETRAStepMax;
  G4UIcmdWithADoubleAndUnit* fDEMETRAStepMaxCmd;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
