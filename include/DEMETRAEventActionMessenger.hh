#ifndef DEMETRAEventActionMessenger_h
#define DEMETRAEventActionMessenger_h 1

//---------------------------------------------------------------------------//

#include "globals.hh"
#include "G4UImessenger.hh"

//---------------------------------------------------------------------------//

class G4UIdirectory;
class G4UIcommand;
class G4UIcmdWithAnInteger;
class DEMETRAEventAction;

//---------------------------------------------------------------------------//

class DEMETRAEventActionMessenger : public G4UImessenger
{
public:

  //default constructor
  DEMETRAEventActionMessenger(DEMETRAEventAction *evact);

  //copy constructor
  //DEMETRAEventActionMessenger();

  //destructor
  ~DEMETRAEventActionMessenger();

  //public interface
  void SetNewValue(G4UIcommand *command, G4String newValues);

  //protected members
protected:

  //private  members
private:
  DEMETRAEventAction        *fEvAct;
  G4UIdirectory        *fDirectory;
  G4UIcmdWithAnInteger *fRepFreqCmd;
};
#endif
