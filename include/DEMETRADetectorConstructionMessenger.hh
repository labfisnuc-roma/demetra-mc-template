#ifndef _DEMETRADETECTORCONSTRUCTIONMESSENGER_HH
#define _DEMETRADETECTORCONSTRUCTIONMESSENGER_HH

//---------------------------------------------------------------------------//

#include "globals.hh"
#include "G4UImessenger.hh"

//---------------------------------------------------------------------------//

class G4UIdirectory;
class G4UIcommand;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWithoutParameter;
class DEMETRADetectorConstruction;

//---------------------------------------------------------------------------//

class DEMETRADetectorConstructionMessenger : public G4UImessenger
{
public:
    
    //default constructor
    DEMETRADetectorConstructionMessenger(DEMETRADetectorConstruction *detector);
    
    //copy constructor
    //DEMETRADetectorConstructionMessenger();
    
    //destructor
    ~DEMETRADetectorConstructionMessenger();
    
    //public interface
    void SetNewValue(G4UIcommand *command, G4String newValues);
    
    //protected members
protected:
    
    //private  members
private:
    DEMETRADetectorConstruction   *fDetectorPrimary;
    
    G4UIdirectory               *fDetectorDirectory;
    G4UIcmdWithAString          *fDEMETRASetupCmd;

  //    G4UIcmdWithAnInteger        *fNumXCmd;
  //    G4UIcmdWithAnInteger        *fNumYCmd;
  //    G4UIcmdWithAnInteger        *fNumZCmd;
    
    G4UIdirectory               *fLabDirectory;
    G4UIcmdWithAString          *fDEMETRALabCmd;
    G4UIcmdWithADoubleAndUnit   *fAirInnerRadiusCmd;
    G4UIcmdWithADoubleAndUnit   *fAirThicknessCmd;
    G4UIcmdWithADoubleAndUnit   *fexternalrockthickCmd;
    G4UIcmdWithADoubleAndUnit   *fproductionrockthickCmd;
    G4UIcmdWithADoubleAndUnit   *finternalrockthickCmd;

    G4UIdirectory               *fShieldDirectory;
    G4UIcmdWithAString          *fDEMETRAShieldingCmd;
    G4UIcmdWithADoubleAndUnit   *fthick0Cmd;
    G4UIcmdWithADoubleAndUnit   *fthick1Cmd;
    G4UIcmdWithADoubleAndUnit   *fthick2Cmd;
    G4UIcmdWithADoubleAndUnit   *fthick3Cmd;
    G4UIcmdWithAString          *fMat0Cmd;
    G4UIcmdWithAString          *fMat1Cmd;
    G4UIcmdWithAString          *fMat2Cmd;
    G4UIcmdWithAString          *fMat3Cmd;

    G4UIcmdWithADoubleAndUnit   *fDetXCmd;
    G4UIcmdWithADoubleAndUnit   *fDetYCmd;
    G4UIcmdWithADoubleAndUnit   *fDetZCmd;
    
  //    G4UIcmdWithAString          *fSourceCmd;
  //    G4UIcmdWithADoubleAndUnit   *fSourcePosCmd;
    
    G4UIcmdWithoutParameter     *fupdateCmd;
};
#endif
