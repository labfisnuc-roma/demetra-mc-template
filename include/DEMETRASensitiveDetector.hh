#ifndef DEMETRASensitiveDetector_h
#define DEMETRASensitiveDetector_h 1

#include "G4VSensitiveDetector.hh"
#include "DEMETRADetectorConstruction.hh"
#include "DEMETRAHit.hh"

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

class DEMETRASensitiveDetector : public G4VSensitiveDetector
{
  public:
      DEMETRASensitiveDetector(G4String name);
      virtual ~DEMETRASensitiveDetector();

      virtual void Initialize(G4HCofThisEvent*);
      virtual G4bool ProcessHits(G4Step*, G4TouchableHistory*);
      virtual void EndOfEvent(G4HCofThisEvent*);

  private:
      DEMETRAHitsCollection* DEMETRACollection;
      DEMETRADetectorConstruction* fDetector;
      G4int fHCID;
};

#endif

