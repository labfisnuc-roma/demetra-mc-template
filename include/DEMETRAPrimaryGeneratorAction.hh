#ifndef DEMETRAPrimaryGeneratorAction_h
#define DEMETRAPrimaryGeneratorAction_h 1

//#include "DEMETRAGeneratorPositionSampling.hh"
#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ThreeVector.hh"
#include <fstream>
#include <stdio.h>
#include <vector>
#include "globals.hh"

class DEMETRAPrimaryGeneratorActionMessenger;
class DEMETRADetectorConstruction;
class DEMETRAAtmosphericMuonGenerator;
class G4GeneralParticleSource;
class G4ParticleGun;
class G4Event;

class DEMETRAPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    DEMETRAPrimaryGeneratorAction(DEMETRADetectorConstruction*);    
   ~DEMETRAPrimaryGeneratorAction();

  public:
  void GeneratePrimaries(G4Event*);  
  void SetEnergy(G4double ene) {particle_energy = ene;}
  G4double GetEnergy() {return particle_energy;}

  void SetGenType(G4String);
  G4String GetGenType() {return fGenType;};

  private:
  G4GeneralParticleSource* particleGunGPS;
  DEMETRAAtmosphericMuonGenerator* particleGunAtmMu ;
  //  G4ParticleGun* particleGun;
  DEMETRADetectorConstruction* myDetector;
  G4int n_particle;
  G4double particle_energy;
  G4String fGenType;
 
  G4ThreeVector position;
  DEMETRAPrimaryGeneratorActionMessenger* fMessenger;
};

#endif


