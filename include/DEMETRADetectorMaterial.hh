#ifndef DEMETRADetectorMaterial_h
#define DEMETRADetectorMaterial_h 1

#include "G4Material.hh"
#include "G4Element.hh"
#include "G4NistManager.hh"
#include "G4VisAttributes.hh"
#include "globals.hh"

class DEMETRADetectorMaterial
{
public:

  DEMETRADetectorMaterial();
  ~DEMETRADetectorMaterial();
  static DEMETRADetectorMaterial* GetInstance();
  G4Material* Material(G4String);
  G4VisAttributes* VisAttributes(G4String);
  G4Material* FindOrBuildMaterial(const G4String& name, G4bool isotopes=true, G4bool warning=false);
  static DEMETRADetectorMaterial* fDEMETRADetectorMaterial;

private:

//**********************************************************************
//   NAME OF MATERIALS
//**********************************************************************

G4NistManager* man;

G4Material* Air;

G4Material* O;
G4Material* Na;
G4Material* K;
G4Material* B;
G4Material* Al;
G4Material* Cu;
G4Material* Fe;
G4Material* Pb;
G4Material* Co;
G4Material* Ni;
G4Material* Si;
G4Material* In;

G4Material* Teflon;
G4Material* PyrexGlass;
G4Material* NaI;
G4Material* PC_Scint;
G4Material* Quartz;
G4Material* Ceramic;
G4Material* Kovar;
G4Material* lngsRock;
G4Material* Vacuum;
G4Material* Water;
G4Material* Steel;
G4Material* PE;
G4Material* Concrete;

G4VisAttributes* PEVis;
G4VisAttributes* WaterVis;
G4VisAttributes* AirVis;
G4VisAttributes* VacuumVis;
G4VisAttributes* CopperVis;

};

#endif   /* DEMETRADetectorMaterial.hh */




