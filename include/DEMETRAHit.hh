#ifndef DEMETRAHit_h
#define DEMETRAHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4Types.hh"
#include "G4ThreeVector.hh"

class DEMETRAHit : public G4VHit
{
  public:

      DEMETRAHit();
     ~DEMETRAHit();
      DEMETRAHit(const DEMETRAHit&);
      const DEMETRAHit& operator=(const DEMETRAHit&);
      G4int operator==(const DEMETRAHit&) const;

      inline void* operator new(size_t);
      inline void  operator delete(void*);

      void Print();

  public:
      void SetParentID  (G4int pid)      { parentID = pid; };
      void SetParticleID  (G4int partID)      { particleID = partID; };
      void SetTrackID  (G4int track)      { trackID = track; };
      void SetGlobalTime  (G4double gtime)      { globalTime = gtime; };
      void SetKineticEne  (G4double kene)      { kinEne = kene; };
      void SetProcessIni  (G4String prini)      { processIni = prini; };
      void SetProcessFin  (G4String prfin)      { processFin = prfin; };
      void SetDetN     (G4int dn)         { detN = dn;};
      void SetEdep     (G4double de)      { edep = de; };
      void SetPos      (G4ThreeVector xyz){ pos = xyz; };
 
      G4int GetParentID() { return parentID; };
      G4int GetParticleID() { return particleID; };
      G4double GetGlobalTime() { return globalTime; };
      G4double GetKineticEne() { return kinEne; };
      G4String GetProcessIni() { return processIni; };
      G4String GetProcessFin() { return processFin; };
      G4int GetTrackID()    { return trackID; };
      G4int GetDetN()       { return detN; };
      G4double GetEdep()    { return edep; };      
      G4ThreeVector GetPos(){ return pos; };
       
  private:
 
      G4int         parentID;
      G4int         particleID;
      G4int         trackID;
      G4double globalTime;
      G4double kinEne;
      G4String processIni;
      G4String processFin;
      G4int         detN;
      G4double      edep;
      G4ThreeVector pos;
 };

typedef G4THitsCollection<DEMETRAHit> DEMETRAHitsCollection;

extern G4ThreadLocal G4Allocator<DEMETRAHit>* DEMETRAHitAllocator;

inline void* DEMETRAHit::operator new(size_t)
{
  if(!DEMETRAHitAllocator) DEMETRAHitAllocator = new G4Allocator<DEMETRAHit>;
  return (void *) DEMETRAHitAllocator->MallocSingle();
}

inline void DEMETRAHit::operator delete(void *aHit)
{
  DEMETRAHitAllocator->FreeSingle((DEMETRAHit*) aHit);
}

#endif
