#ifndef DEMETRASteppingAction_h
#define DEMETRASteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "DEMETRADetectorConstruction.hh"
#include "DEMETRAEventAction.hh"
#include "DEMETRAAnalysis.hh"

class DEMETRASteppingAction : public G4UserSteppingAction
{
  public:
	  DEMETRASteppingAction(DEMETRADetectorConstruction*, DEMETRAEventAction*);
	  ~DEMETRASteppingAction(){};

	  void UserSteppingAction(const G4Step*);
  private:

	  DEMETRADetectorConstruction* fDetector;
	  DEMETRAEventAction* fEventAction;
};


#endif
