#ifndef DEMETRADetectorRENOIR_h
#define DEMETRADetectorRENOIR_h 1

#include "globals.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4AssemblyVolume.hh"

class DEMETRADetectorRENOIRMessenger;

class DEMETRADetectorRENOIR
{
public:

  DEMETRADetectorRENOIR();
  ~DEMETRADetectorRENOIR();
  static DEMETRADetectorRENOIR* GetInstance();
  void Construct();
  void ConstructRENOIR();
  void UpdateGeometry();
  void SaveMassAndDensity();
  void Refresh();

  G4LogicalVolume* GetDetector() {return RENOIRDetector_log;}
  G4LogicalVolume* GetPbShield() {return PbShield_log;}
  G4AssemblyVolume* GetAssembly() {return assemblyDetector;}

  static DEMETRADetectorRENOIR* fDEMETRADetectorRENOIR;

  //  G4double rockdist_z;
  //  G4double rockdepth_z;

private:

  DEMETRADetectorRENOIRMessenger* fMessenger;

  G4LogicalVolume* PbShield_log;
  G4LogicalVolume* RENOIRDetector_log;
  G4AssemblyVolume* assemblyDetector;

};

#endif   /* DEMETRADetectorRENOIR.hh */




