#ifndef DEMETRAAtmosphericMuonGenerator_h
#define DEMETRAAtmosphericMuonGenerator_h 1

//#include "DEMETRAGeneratorPositionSampling.hh"
#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ThreeVector.hh"
#include <fstream>
#include <stdio.h>
#include <vector>
#include "globals.hh"

//class DEMETRAPrimaryGeneratorActionMessenger;
class DEMETRADetectorConstruction;
class G4ParticleGun;
class G4Event;

class DEMETRAAtmosphericMuonGenerator : public G4VUserPrimaryGeneratorAction
{
  public:
    DEMETRAAtmosphericMuonGenerator(DEMETRADetectorConstruction*);    
   ~DEMETRAAtmosphericMuonGenerator();

  public:
  void GeneratePrimaries(G4Event*);  
  void SetEnergy(G4double ene) {particle_energy = ene;}
  G4double GetEnergy() {return particle_energy;}
  G4double GenerateMuonEnergy();
  G4double GenerateMuonDirection();

  private:
  G4ParticleGun* particleGun;
  DEMETRADetectorConstruction* myDetector;
  G4int n_particle;
  G4double particle_energy;
 
  G4ThreeVector position;
//  DEMETRAPrimaryGeneratorActionMessenger* fMessenger;

  unsigned nBinE = 10000;
  unsigned nBinTheta = 10000;
  G4double PI = 3.1416;
  G4double EminMu = 0.5; // 0.5 GeV
  G4double EmaxMu = 10000.5; // 10 TeV
  G4double Estep = (EmaxMu-EminMu)/nBinE; // 1 GeV
  G4double ThetaminMu = 0.; 
  G4double ThetamaxMu = PI/2.;  
  G4double Thetastep = (ThetamaxMu-ThetaminMu)/nBinTheta;

  G4double Izero = 70.7; 
  G4double exp_n = 3.01; 
  G4double Ezero = 4.29;
  G4double epsilon = 854.; 
  G4double NormE =1./1.5;   
  G4double NormTheta =1./0.500254; 
  
};

#endif


