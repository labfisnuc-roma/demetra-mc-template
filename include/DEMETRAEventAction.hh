#ifndef DEMETRAEventAction_h
#define DEMETRAEventAction_h 1

#include "G4UserEventAction.hh"
#include "DEMETRARunAction.hh"

class G4Event;
class DEMETRAEventActionMessenger;
class DEMETRADetectorConstruction;

class DEMETRAEventAction : public G4UserEventAction
{
  public:
  DEMETRAEventAction(DEMETRARunAction*,DEMETRADetectorConstruction*);
   ~DEMETRAEventAction();

  public:
  void BeginOfEventAction(const G4Event* evt);
  void EndOfEventAction(const G4Event* evt);

  void SetDetectorHit(bool isHit){fDetectorHit = isHit;};
  void SetRepFreq(G4int rfreq) {repfreq = rfreq;}

  private: 
  
  DEMETRAEventActionMessenger* fMessenger;
  DEMETRARunAction*  fRunAct;
  DEMETRADetectorConstruction* fDetector;
  
  bool fDetectorHit;
  G4int repfreq;
};

#endif


