# DEMETRA

Welcome to DEMETRA, 
the multi purpose simulation package developed and maintained by the INFN RM1 Simulation Group

The prerequisite to run this simulations is to have ROOT (v6.08.02) and GEANT4 (v10.05) software installed.
For more informations about the software see https://root.cern.ch/ and http://geant4.cern.ch/

## To setup all the environment variables of ROOT and GEANT4:
```
source path-to-root-install/bin/thisroot.sh
source path-to-geant-install/bin/geant.sh
```
You can put these commands in your .bashrc to execute them automatically every time you open a bash shell.

## To download the DEMETRA repository:

```
git clone git@gitlab.com:labfisnuc-roma/demetra-mc-template.git
```
or, if you don't want to configure a ssh key in baltig use http protocol
```
git clone https://gitlab.com/labfisnuc-roma/demetra-mc-template.git
```
Now you have downloaded the code in demetra-mc-template/ directory.

## Compile DEMETRA

Create a build directory, enter the build directory and compile with:
 
```
cmake3 -DGeant4_DIR=<path-to-your-geant4-install>/lib/Geant4-XX.X.X/ <path-to-your-demetra-mc-template>
make -j`nproc`
```

## Run DEMETRA

For the interactive mode just run the executable
```
./DEMETRA 
```
If you want to execute a macro run:
```
./DEMETRA mymacro.mac
```


## Guide to commands

**DETECTOR**

*/DEMETRA/detector/setup* Set the detector to be used in the multi-purpose DEMETRA simulation. Possible choices: DEMETRA, RENOIR

*/DEMETRA/detector/detx*
*/DEMETRA/detector/dety*
*/DEMETRA/detector/detz* 
Set the dimensions for the DEMETRA detector

*/DEMETRA/lab/* Select the DEMETRA laboratory geometry. Possible choices: LNGS or NoCave

*/DEMETRA/lab/airinnerradius*
*/DEMETRA/lab/airthickness*
Set inner radius and thickness for air sphere in the NoCave option .These commands are valid only if DEMETRALab has been selected as NoCave

*/DEMETRA/lab/externalrockthick* Set thickness for external (outermost) rock layer

*/DEMETRA/lab/productionlayerthick* Set thickness for production layer for neutron generation

*/DEMETRA/lab/internalrockthick* Set thickness for internal (innermost) rock layer, i.e. starting face of production layer.
These commands are valid only if DEMETRALab has been selected as LNGS

*/DEMETRA/shield/select* Select the DEMETRA shielding geometry. Possible choices: Full, NoShield.
Full: four boxes of different thicknesses and materials. Shielding thickness and material to be selected by user, NoShield: no shielding

*/DEMETRA/shield/thick0*
*/DEMETRA/shield/thick1*
*/DEMETRA/shield/thick2*
*/DEMETRA/shield/thick3*
Set thickness for shield layer #0; layers are numbered from 0 to 3, #0 being the outermost

*/DEMETRA/shield/mat0*
*/DEMETRA/shield/mat1*
*/DEMETRA/shield/mat2*
*/DEMETRA/shield/mat3*
Set material of the DEMETRA shield layer #3. Possible candidates: Pb, PE, Cu, Air, Water. These commands are valid only if DEMETRAShielding has been selected as Full

*/DEMETRA/updateGeo* Update geometry

**RUN**

*/DEMETRA/reportingfrequency* Set the reporting frequency (default every 1000 events)

*/DEMETRA/gentype* Set the generator type. Possible choices: GPS, AtmosphericMuons (default GPS)

*/DEMETRA/outfile* Set the output file name without extension (default out --> out.root)

*/DEMETRA/cutoutfile* If 0 saves all the events; if 1 saves only events with energy deposited in the detector (default)

*/DEMETRA/registeron* If 1 saves flux, neutron and isotope data; if 0 does not save these data (default)

*/DEMETRA/tottree* If 0 does not save the tree containing the generation information for each event; if 1 saves these data (default)

*/DEMETRA/save_hits_branches* If 0 does not save the tree branches containing the hits information for each event (default); if 1 saves these data 

*/DEMETRA/stepmax"* Set max allowed step length.

## Structure of output file

The simulation produces an output root file (default filename out.root), containing a tree `nT` with the following variables:

*eventnumber* event number

*numvertex* number of generated vertexes for each event

*numparticles* number of generated particles 

*numhitsDet* number of hits in the detector

*numdets* number of detectors 

*neutronflag* flag to identify neutrons

*muonflag* flag to identify muons

*inelasticflag* flag to identify hadrons

*edepDet* energy deposited in the detector (summed over hits)

*xpos_vertex, ypos_vertex, zpos_vertex* vertex coordinates

*time_vertex* vertex time (0 for now)

*numparticle_vertex* number of generated particles for each vertex

*pdgid_particle* PDG identifier for the particle

*ivertex_particle* vertex identifier for the particle

*px_particle, py_particle, pz_particle* particle momentum components

*ekin_particle* kinetic energy of the particle

*etot_particle* total energy of the particle

*impact_param_particle* impact parameter of the particle

*direct_angle_particle* direction angle of the particle

*edepDet_N* not to be used now (needs more than 1 detector) 

*ndet_hits* not to be used now (needs more than 1 detector)

*parentID_hits* parentID of each hit

*pdgID_hits* PDG identifier of each hit

*trackID_hits* trackID of each hit

*time_hits* time of each hit

*kinEne_hits* kinetic energy of each hit

*x_hits, y_hits, z_hits* hit coordinates

*x_vertex_hits, y_vertex_hits, z_vertex_hits* not to be used now

*edepDet_hits* deposited energy of each hit

*id_det* not to be used now (needs more than 1 detector) 
