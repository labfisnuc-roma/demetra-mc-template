#include "DEMETRAStepMaxMessenger.hh"

#include "DEMETRAStepMax.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DEMETRAStepMaxMessenger::DEMETRAStepMaxMessenger(DEMETRAStepMax* stepM)
  :G4UImessenger(),fDEMETRAStepMax(stepM),fDEMETRAStepMaxCmd(0)
{ 
  fDEMETRAStepMaxCmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/stepmax",this);
  fDEMETRAStepMaxCmd->SetGuidance("Set max allowed step length");
  fDEMETRAStepMaxCmd->SetParameterName("mxStep",false);
  fDEMETRAStepMaxCmd->SetRange("mxStep>0.");
  fDEMETRAStepMaxCmd->SetUnitCategory("Length");
  fDEMETRAStepMaxCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DEMETRAStepMaxMessenger::~DEMETRAStepMaxMessenger()
{
  delete fDEMETRAStepMaxCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DEMETRAStepMaxMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{ 
  if (command == fDEMETRAStepMaxCmd)
    { fDEMETRAStepMax->SetMaxStep(fDEMETRAStepMaxCmd->GetNewDoubleValue(newValue));}
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
