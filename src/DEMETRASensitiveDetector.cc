#include "DEMETRASensitiveDetector.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4VProcess.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

DEMETRASensitiveDetector::DEMETRASensitiveDetector(G4String name)
  :G4VSensitiveDetector(name), fHCID(-1)
{
  G4String HCname;
  collectionName.insert(HCname="DEMETRACollection");
}

DEMETRASensitiveDetector::~DEMETRASensitiveDetector(){ }

void DEMETRASensitiveDetector::Initialize(G4HCofThisEvent* HCE)
{
  DEMETRACollection = new DEMETRAHitsCollection(SensitiveDetectorName,collectionName[0]); 
  if(fHCID<0)
  { fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(DEMETRACollection); }
  HCE->AddHitsCollection( fHCID, DEMETRACollection ); 
}

G4bool DEMETRASensitiveDetector::ProcessHits(G4Step* aStep,G4TouchableHistory*)
{
  G4double edep = aStep->GetTotalEnergyDeposit();
  if(edep==0.) return true;
 
  DEMETRAHit* newHit = new DEMETRAHit();
  // parent ID
  newHit->SetParentID  (aStep->GetTrack()->GetParentID());
  // particle ID
  newHit->SetParticleID  (aStep->GetTrack()->GetDefinition()->GetPDGEncoding());
  // track ID
  newHit->SetTrackID  (aStep->GetTrack()->GetTrackID());
  // global time
  newHit->SetGlobalTime  (aStep->GetTrack()->GetGlobalTime());
  // kinetic energy
  newHit->SetKineticEne  (aStep->GetPreStepPoint()->GetKineticEnergy());

  if (aStep->GetPreStepPoint()->GetProcessDefinedStep() != NULL) {
    newHit->SetProcessIni  (aStep->GetPreStepPoint()->GetProcessDefinedStep()->GetProcessName());
  }
  else  newHit->SetProcessIni  ("none");

  newHit->SetProcessFin  (aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName());

  G4int depth = 0;
  G4String volumeName = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetName();
  G4int volumeNumber = aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber(depth);
  newHit->SetDetN     (volumeNumber);
  newHit->SetEdep     (edep);
  newHit->SetPos      (aStep->GetPostStepPoint()->GetPosition());
  DEMETRACollection->insert( newHit );
  return true;
}

void DEMETRASensitiveDetector::EndOfEvent(G4HCofThisEvent*)
{
  verboseLevel=0;

  if (verboseLevel>0) { 
    G4int NbHits = DEMETRACollection->entries();
    if(NbHits>0)
      {
	G4cout << "\n-------->Hits Collection: in this event there are " << NbHits 
	       << " hits in DEMETRA detector: " << G4endl;
	for (G4int i=0;i<NbHits;i++) (*DEMETRACollection)[i]->Print();   
      } 
  }
}

