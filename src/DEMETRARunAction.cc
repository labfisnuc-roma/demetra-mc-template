#include "DEMETRARunAction.hh"
#include "DEMETRARunActionMessenger.hh"
#include "DEMETRAAnalysis.hh"
#include "DEMETRADetectorConstruction.hh"

#include "DEMETRARun.hh"

DEMETRARunAction::DEMETRARunAction(DEMETRADetectorConstruction* myDC)
  : G4UserRunAction(), fDetector(myDC)
{
  fMessenger = new DEMETRARunActionMessenger(this);
  FileName = "out";
  fOutFileCut = 1;
  fRegisterOn = 0;
  fHitsInfo = 0;
  fTotT = 1;
}

DEMETRARunAction::~DEMETRARunAction()
{
  delete G4AnalysisManager::Instance();
  DEMETRAAnalysis* analysis = DEMETRAAnalysis::getInstance();
  delete analysis; 
  delete fMessenger;
}

void DEMETRARunAction::BeginOfRunAction(const G4Run* aRun)
{
	G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;

	//inform the runManager to save random number seed
	//G4RunManager::GetRunManager()->SetRandomNumberStore(true);
	
	// Get analysis manager
	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

	DEMETRAAnalysis* analysis = DEMETRAAnalysis::getInstance();
	analysis->SetOutFileCut(fOutFileCut);
	analysis->SetRegisterOn(fRegisterOn);
	analysis->SetTotT(fTotT);
	analysis->SetHitsInfo(fHitsInfo);
	analysis->InitRun(FileName,fDetector);
	// Open an output file
	// The default file name is set in RunAction::RunAction(),
	// it can be overwritten in a macro
	analysisManager->OpenFile();
}

void DEMETRARunAction::EndOfRunAction(const G4Run*)
{ 
  
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->Write();
  analysisManager->CloseFile();

}

G4Run* DEMETRARunAction::GenerateRun() {
    return new DEMETRARun;
}

