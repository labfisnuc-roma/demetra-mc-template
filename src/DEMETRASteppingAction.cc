#include "DEMETRASteppingAction.hh"
#include "DEMETRAVolumes.hh"
#include "G4SteppingManager.hh"
#include "G4Track.hh"
#include "globals.hh"
#include "G4Ions.hh"
#include "G4Neutron.hh"
#include "G4Proton.hh"
#include "G4MuonMinus.hh"
#include "G4MuonPlus.hh"
#include "G4Gamma.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

DEMETRASteppingAction::DEMETRASteppingAction(DEMETRADetectorConstruction* det, DEMETRAEventAction* evt ):
fDetector(det), fEventAction(evt){ }

void DEMETRASteppingAction::UserSteppingAction(const G4Step* fStep)
{ 

  // get analysis instance
  DEMETRAAnalysis* analysis = DEMETRAAnalysis::getInstance();

 // get volume of the current step
  G4VPhysicalVolume* volume = fStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume();
  G4VPhysicalVolume* nextvolume = fStep->GetPostStepPoint()->GetTouchableHandle()->GetVolume();
  G4String name = volume->GetName();
  G4String nextname = "OutOfWorld";
  if(nextvolume) nextname=nextvolume->GetName();

  G4Track* fTrack = fStep->GetTrack();
  G4int StepNo = fTrack->GetCurrentStepNumber();

  if (fTrack->GetDefinition()->GetParticleType() == "nucleus" && fStep->GetTrack()->GetParentID()>0)
    {
      //G4cout <<"StepNo "<<StepNo<<" secondary nucleus "<<fTrack->GetDefinition()->GetParticleName()<<G4endl;
      G4double energy = fTrack->GetKineticEnergy();
      if (energy < 0.1*keV) // FIXME: check this value of energy
        {
          G4Ions* ion = (G4Ions*) fTrack->GetDefinition();
          G4double lifetime = ion->GetPDGLifeTime();
          G4double excitationEnergy = ion->GetExcitationEnergy();

          //stable and excited nuclei --> track them as usual 
	  //if (lifetime < 0 || excitationEnergy > 0) return;
          if (lifetime < 0 || (excitationEnergy > 0 && fTrack->GetDefinition()->GetParticleName()!="Pa234[73.920]")) return;
	 
          //                                                                                             
	            if (lifetime > 1.0*microsecond) //kill long-lived nuclei
	              {
	                G4String particleName = fTrack->GetDefinition()->GetParticleName();
	                // old killing
	  	      //fTrack->SetTrackStatus(fStopAndKill);
	  	      // new killing
	  	      //Notice: the StepAction is not called at step#0. Secondaries
	  	      //are generated if RadioactiveDecay takes place at step#1
	  	      G4TrackStatus newstatus =
	  	      (fTrack->GetCurrentStepNumber() > 1) ?
	  	      fStopAndKill : fKillTrackAndSecondaries;
	  	      fTrack->SetTrackStatus(newstatus);
	  	    }
	  
	  //  else if (lifetime < 1.0*microsecond && lifetime > 0) //decay short-lived nuclei            
	  //        {                                                                                    
	  //          G4String particleName = fStep->GetTrack()->GetDefinition()->GetParticleName();     
	  //          fStep->GetTrack()->SetTrackStatus(fStopButAlive);                                  
	  //          //G4cout << "Allows decay of track: " << particleName << " (life time: " <<        
	  //          //        lifetime/s << " s)" << G4endl;                                           
	  //        }                                                                                    

	  //stable and short-lived nuclei are unaffected                                                 
	}
    }

  // Get the particles current across a 61.cm radius sphere surrounding the crystals. Assuming crystals to be placed around the center of the global ref frame origin
  // G4double sphereRadius = 63.0*cm;
  G4int trackID = fTrack->GetTrackID();
  G4int preVolNo = analysis->GetPreVolNo(fTrack);
  G4int nextVolNo = analysis->GetVolNo(fTrack);
  G4int nextCopyNo = analysis->GetCopyNo(fTrack);
  G4int PDGcode = fTrack->GetDefinition()->GetPDGEncoding();
  G4ThreeVector preStepPoint = fStep->GetPreStepPoint()->GetPosition();
  G4ThreeVector postStepPoint = fStep->GetPostStepPoint()->GetPosition();
  G4LorentzVector quadriMom = fTrack->GetDynamicParticle()->Get4Momentum();


  if(//(name=="externalRock" && nextname=="expHall") ||
     //(name=="expHall" && nextname=="AirHemisphere") || 
     //(name=="expHall" && nextname=="Shield0") || 
     //(name=="InnerAirSphere" && nextname=="Shield0") || 
     //(name=="Shield0" && nextname=="Shield1") || 
     //(name=="Shield1" && nextname=="Shield2") || 
     //(name=="Shield2" && nextname=="Shield3") || 
     //(name=="Shield3" && nextname=="AirBox"))
     (name=="AirBox" && nextname=="Detector") ||  // for gdml vessel geometry
     (name!="Shield0" && nextname=="Shield0") || 
     (name!="Shield1" && nextname=="Shield1") || 
     (name!="Shield2" && nextname=="Shield2") || 
     (name!="Shield3" && nextname=="Shield3") || 
     (name!="AirBox" && nextname=="AirBox") || 	 
     (name!="Detector" && nextname=="Detector")) 
     {

    analysis->RegisterParticle(trackID, preVolNo, nextVolNo, nextCopyNo, PDGcode, preStepPoint, postStepPoint, quadriMom);
    
  }

//  if (preStepPoint.mag() >= sphereRadius && postStepPoint.mag() < sphereRadius){
//    G4int sphereNo = SPHERE;
//    G4int sphereCopyNo = -11;
//    analysis->RegisterParticle(sphereNo, sphereCopyNo, PDGcode, preStepPoint, postStepPoint, quadriMom);
//  }

  
  // Check for neutrons in Crystals
  if (fTrack->GetDefinition() == G4Neutron::NeutronDefinition())
    {
      G4double neuEnergy = fTrack->GetKineticEnergy();
      if (fTrack->GetMaterial()->GetName() == "NaI")
        {
	  G4ThreeVector postStpPt = fTrack->GetPosition();
	  G4LorentzVector fourMom = fTrack->GetDynamicParticle()->Get4Momentum();
	  analysis->RegisterNeutron(fTrack->GetTrackID(), fTrack->GetParentID(), postStpPt, fourMom);
	  analysis->SetNeutronFlag(1);
        }
      if (neuEnergy > 19.999*MeV && neuEnergy < 20.001*MeV)
        {
	  fTrack->SetKineticEnergy(19.99*MeV);
	  //this is due to the fact that 20-MeV neutrons may enter in an infinite loop
        }
    }
  if (fTrack->GetDefinition() == G4Proton::Definition())
    {
      if (fTrack->GetMaterial()->GetName() == "NaI")
        {
	  analysis->SetNeutronFlag(1);
        }
    }
  if (fTrack->GetDefinition() == G4MuonMinus::Definition() ||
      fTrack->GetDefinition() == G4MuonPlus::Definition())
    {
      if (fTrack->GetMaterial()->GetName() == "NaI")
        {
	  analysis->SetMuonFlag(1);
        }
    }
  if ((fTrack->GetMaterial()->GetName() == "NaI") &&
      (fTrack->GetDefinition() == G4Gamma::Definition()))
    {
      if (fTrack->GetParentID()>0)
        {
	  G4String processname = fTrack->GetCreatorProcess()->GetProcessName();
	  if (processname == "nCapture" || processname == "nInelastic")
            {
	      analysis->SetInelasticFlag(1);
            }
        }
    }

  if(StepNo >= 10000) fTrack->SetTrackStatus(fStopAndKill);
}

