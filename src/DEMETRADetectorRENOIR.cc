/* ********************************************************
 * INFN RM1 GEANT4 SIMULATION GROUP 
 *
 * File:      DetectorRENOIR.cc
 *
 * Defines the experimental setup and detector of RENOIR
 *
 **********************************************************/

#include "DEMETRADetectorRENOIR.hh"
#include "DEMETRADetectorRENOIRMessenger.hh"
#include "DEMETRADetectorMaterial.hh"

#include "G4VisAttributes.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4EllipticalTube.hh"
#include "G4Ellipsoid.hh"
#include "G4IntersectionSolid.hh"
#include "G4Polycone.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4UnionSolid.hh"
#include "G4AssemblyVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4SubtractionSolid.hh"
#include "G4PVDivision.hh"
#include "G4SDManager.hh"


DEMETRADetectorRENOIR* DEMETRADetectorRENOIR::fDEMETRADetectorRENOIR = NULL;

DEMETRADetectorRENOIR::DEMETRADetectorRENOIR() 
//:
// Rock_log(0),
//  size_Rock() 
{

  fMessenger = new DEMETRADetectorRENOIRMessenger(this);
  Construct();
  
}

DEMETRADetectorRENOIR::~DEMETRADetectorRENOIR()
{
  delete fMessenger;
}

DEMETRADetectorRENOIR* DEMETRADetectorRENOIR::GetInstance()
{
  if (fDEMETRADetectorRENOIR == NULL) {
    fDEMETRADetectorRENOIR = new DEMETRADetectorRENOIR();
  }
  return fDEMETRADetectorRENOIR;
}

void DEMETRADetectorRENOIR::UpdateGeometry()
{
  Construct();
}

void DEMETRADetectorRENOIR::Construct()
{  
  ConstructRENOIR();
  //ConstructShielding();
}

void DEMETRADetectorRENOIR::ConstructRENOIR()
{

  //G4cout << "Constructing materials...";
  DEMETRADetectorMaterial* DEMETRAMaterials = DEMETRADetectorMaterial::GetInstance();
  //G4cout << "... done" << G4endl;

  //  absrot_Shielding = G4RotationMatrix();

  // the main unit of the RENOIR detector is a cylindrical plastic vial, filled up to 2 cm with a liquid (water density) to feed the drosophila

  G4double liquid_r = 11.*mm;
  G4double liquid_h = 20.*mm;

  G4double vial_ir = 11.001*mm;
  G4double vial_or = 11.5*mm;
  G4double vial_h = 95.*mm;
  
  G4double gap = 50.*mm;

  //Name of the volumes

  G4String name_phys="RENOIRDetector";
  G4String name_log=name_phys+"_log";
  G4String name_solid=name_phys+"_solid";

  G4ThreeVector tr_liquid;
  tr_liquid = G4ThreeVector(0.,0.,(-vial_h+liquid_h)/2.+1.*mm);//translation for the liquid volume
  G4RotationMatrix rot_liquid;
  rot_liquid = G4RotationMatrix();

  //  size_InsideVolume = G4ThreeVector(0.,0.,0.);
  //  size_Shielding = G4ThreeVector(0.,0.,0.);
  
  //  Shielding_log = 0;  
  //  InsideVolume_log = 0;

  G4Tubs* Vial_solid = new G4Tubs("Vial_solid",vial_ir,vial_or,0.5*vial_h,0.*deg,360.*deg);
  G4Tubs* Liquid_solid = new G4Tubs("Liquid_solid",0.,liquid_r,0.5*liquid_h,0.*deg,360.*deg);
  G4LogicalVolume* Vial_log = new G4LogicalVolume(Vial_solid,DEMETRAMaterials->Material("Teflon"),"Vial_log",0,0,0);  
  G4LogicalVolume* Liquid_log = new G4LogicalVolume(Liquid_solid,DEMETRAMaterials->Material("Water"),name_log,0,0,0);  
  G4PVPlacement* Liquid_phys = new G4PVPlacement(G4Transform3D(rot_liquid,tr_liquid),Liquid_log,"Liquid",Vial_log,false,0,true);  

  //  RENOIRDetector_log = new G4LogicalVolume(RENOIRDetector,DEMETRAMaterials->Material("Teflon"),name_log,0,0,0);  

  RENOIRDetector_log = Vial_log;

  // Define one vial (with liquid inside) as one assembly volume
  assemblyDetector = new G4AssemblyVolume();

  // Rotation and translation of a plate inside the assembly
  G4RotationMatrix* Ra;
  Ra = new G4RotationMatrix(); 

  G4ThreeVector Ta(0.,0.,0.);
 
 // Set the positions of the vials 
  Ta.setX( gap/2. ); Ta.setY( gap/2. ); Ta.setZ( 0. );
  assemblyDetector->AddPlacedVolume( Vial_log,Ta,Ra );
  Ta.setX( -gap/2. ); Ta.setY( -gap/2. ); Ta.setZ( 0. );
  assemblyDetector->AddPlacedVolume( Vial_log,Ta,Ra );
  Ta.setX( -gap/2. ); Ta.setY( gap/2. ); Ta.setZ( 0. );
  assemblyDetector->AddPlacedVolume( Vial_log,Ta,Ra );
  Ta.setX( gap/2. ); Ta.setY( -gap/2. ); Ta.setZ( 0. );
  assemblyDetector->AddPlacedVolume( Vial_log,Ta, Ra );

  // define lead shielding  

  G4double Pb_IR[] = {0.*cm,0.*cm,14.40*cm,14.40*cm,0.*cm,0.*cm};
  G4double Pb_OR[] = {25.*cm,25.*cm,25.*cm,25.*cm,25.*cm,25.*cm};
  G4double Pb_Z[] = {-20.5*cm,-11.5*cm,-11.49*cm,11.49*cm,11.5*cm,20.5*cm};

  G4Polycone* PbShield_solid = new G4Polycone("PbShieldSolid", 0.*deg, 360.*deg, 6, Pb_Z, Pb_IR, Pb_OR);	
  PbShield_log = new G4LogicalVolume(PbShield_solid,DEMETRAMaterials->Material("Pb"),"PbShield_log",0,0,0);

  PbShield_log->SetVisAttributes(new G4VisAttributes(G4Colour(137./255., 89./255., 86./255.)));    
  Vial_log->SetVisAttributes(new G4VisAttributes(G4Colour(229./255.,234./255.,237./255.)));    
  Liquid_log->SetVisAttributes(new G4VisAttributes(G4Colour(202./255.,209./255.,238./255.)));    
}

void DEMETRADetectorRENOIR::SaveMassAndDensity()
{
}

void DEMETRADetectorRENOIR::Refresh()
{  
  RENOIRDetector_log = 0;
}

