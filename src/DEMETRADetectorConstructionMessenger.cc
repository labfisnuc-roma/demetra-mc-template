#include "globals.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIdirectory.hh"
#include "G4PhysicalVolumeStore.hh"

#include "DEMETRADetectorConstruction.hh"
#include "DEMETRADetectorConstructionMessenger.hh"

//---------------------------------------------------------------------------//

DEMETRADetectorConstructionMessenger::DEMETRADetectorConstructionMessenger
(DEMETRADetectorConstruction *detector):fDetectorPrimary(detector)
{
  fDetectorDirectory = new G4UIdirectory("/DEMETRA/detector/");
  fDetectorDirectory->SetGuidance("Control commands for detector:");
  
  fDEMETRASetupCmd = new G4UIcmdWithAString("/DEMETRA/detector/setup",this);
  fDEMETRASetupCmd->SetGuidance("Set the detector to be used in the multi-purpose DEMETRA simulation. Possible choices: DEMETRA, RENOIR");
  fDEMETRASetupCmd->SetCandidates("DEMETRA RENOIR");
  fDEMETRASetupCmd->SetParameterName("choice",false);
  fDEMETRASetupCmd->AvailableForStates(G4State_Init,G4State_Idle);

  fDetXCmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/detector/detx",this);
  fDetXCmd->SetGuidance("Set the X dimension for the DEMETRA detector");
  fDetXCmd->SetDefaultUnit("mm");
  fDetXCmd->SetUnitCandidates("mm cm m");
  fDetXCmd->SetParameterName("choice",false);
  fDetXCmd->AvailableForStates(G4State_Init,G4State_Idle);

  fDetYCmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/detector/dety",this);
  fDetYCmd->SetGuidance("Set the Y dimension for the DEMETRA detector");
  fDetYCmd->SetDefaultUnit("mm");
  fDetYCmd->SetUnitCandidates("mm cm m");
  fDetYCmd->SetParameterName("choice",false);
  fDetYCmd->AvailableForStates(G4State_Init,G4State_Idle);
 
  fDetZCmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/detector/detz",this);
  fDetZCmd->SetGuidance("Set the Z dimension for the DEMETRA detector");
  fDetZCmd->SetDefaultUnit("mm");
  fDetZCmd->SetUnitCandidates("mm cm m");
  fDetZCmd->SetParameterName("choice",false);
  fDetZCmd->AvailableForStates(G4State_Init,G4State_Idle);

    // to be used in future extensions in case we need an array
  
    /*    
	  fNumXCmd = new G4UIcmdWithAnInteger("/DEMETRA/detector/numX",this);
    fNumXCmd->SetGuidance("Set the number of crystals in the X direction");
    fNumXCmd->SetGuidance("(default: 5)");
    fNumXCmd->SetParameterName("choice",false);
    fNumXCmd->SetRange("choice>0");
    
    fNumYCmd = new G4UIcmdWithAnInteger("/DEMETRA/detector/numY",this);
    fNumYCmd->SetGuidance("Set the number of crystals in the Y direction");
    fNumYCmd->SetGuidance("(default: 5)");
    fNumYCmd->SetParameterName("choice",false);
    fNumYCmd->SetRange("choice>0");
    
    fNumZCmd = new G4UIcmdWithAnInteger("/DEMETRA/detector/numZ",this);
    fNumZCmd->SetGuidance("Set the number of crystals in the Z direction");
    fNumZCmd->SetGuidance("(default: 5)");
    fNumZCmd->SetParameterName("choice",false);
    fNumZCmd->SetRange("choice>0");
    */

  fLabDirectory = new G4UIdirectory("/DEMETRA/lab/");
  fLabDirectory->SetGuidance("Control commands for lab:");
  
  fDEMETRALabCmd = new G4UIcmdWithAString("/DEMETRA/lab/select",this);
  fDEMETRALabCmd->SetGuidance("Select the DEMETRA laboratory geometry. Possible choices: LNGS or NoCave");
  fDEMETRALabCmd->SetGuidance("LNGS: Set the laboratory geometry of LNGS");
  fDEMETRALabCmd->SetGuidance("NoCave: Remove the laboratory and replaces it with an air sphere");
  fDEMETRALabCmd->SetDefaultValue("NoCave");
  fDEMETRALabCmd->SetCandidates("LNGS NoCave");
  fDEMETRALabCmd->SetParameterName("choice",false);
  fDEMETRALabCmd->AvailableForStates(G4State_Init,G4State_Idle);
  
  fAirInnerRadiusCmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/lab/airinnerradius",this);
  fAirInnerRadiusCmd->SetGuidance("Set inner radius for air sphere in the NoCave option");
  fAirInnerRadiusCmd->SetGuidance("This command is valid only if DEMETRALab has been selected as NoCave");
  fAirInnerRadiusCmd->SetDefaultUnit("cm");
  fAirInnerRadiusCmd->SetUnitCandidates("mm cm m");
  fAirInnerRadiusCmd->SetParameterName("choice",false);
  fAirInnerRadiusCmd->AvailableForStates(G4State_Init,G4State_Idle);

  fAirThicknessCmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/lab/airthickness",this);
  fAirThicknessCmd->SetGuidance("Set thickness for air sphere in the NoCave option");
  fAirThicknessCmd->SetGuidance("This command is valid only if DEMETRALab has been selected as NoCave");
  fAirThicknessCmd->SetDefaultUnit("cm");
  fAirThicknessCmd->SetUnitCandidates("mm cm m");
  fAirThicknessCmd->SetParameterName("choice",false);
  fAirThicknessCmd->AvailableForStates(G4State_Init,G4State_Idle);

  fexternalrockthickCmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/lab/externalrockthick",this);
  fexternalrockthickCmd->SetGuidance("Set thickness for external (outermost) rock layer");
  fexternalrockthickCmd->SetGuidance("This command is valid only if DEMETRALab has been selected as LNGS");
  fexternalrockthickCmd->SetDefaultUnit("m");
  fexternalrockthickCmd->SetUnitCandidates("mm cm m");
  fexternalrockthickCmd->SetParameterName("choice",false);
  fexternalrockthickCmd->AvailableForStates(G4State_Init,G4State_Idle);

  fproductionrockthickCmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/lab/productionlayerthick",this);
  fproductionrockthickCmd->SetGuidance("Set thickness for production layer for neutron generation");
  fproductionrockthickCmd->SetGuidance("This command is valid only if DEMETRALab has been selected as LNGS");
  fproductionrockthickCmd->SetDefaultUnit("cm");
  fproductionrockthickCmd->SetUnitCandidates("mm cm m");
  fproductionrockthickCmd->SetParameterName("choice",false);
  fproductionrockthickCmd->AvailableForStates(G4State_Init,G4State_Idle);
    
  finternalrockthickCmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/lab/internalrockthick",this);
  finternalrockthickCmd->SetGuidance("Set thickness for internal (innermost) rock layer, i.e. starting face of production layer");
  finternalrockthickCmd->SetGuidance("This command is valid only if DEMETRALab has been selected as LNGS");
  finternalrockthickCmd->SetDefaultUnit("cm");
  finternalrockthickCmd->SetUnitCandidates("mm cm m");
  finternalrockthickCmd->SetParameterName("choice",false);
  finternalrockthickCmd->AvailableForStates(G4State_Init,G4State_Idle);

  
  fShieldDirectory = new G4UIdirectory("/DEMETRA/shield/");
  fShieldDirectory->SetGuidance("Control commands for shield:");
    
  fDEMETRAShieldingCmd = new G4UIcmdWithAString("/DEMETRA/shield/select",this);
  fDEMETRAShieldingCmd->SetGuidance("Select the DEMETRA shielding geometry. Possible choices: Full, NoShield");
  fDEMETRAShieldingCmd->SetGuidance("Full: four boxes of different thicknesses and materials. Shielding thickness and material to be selected by user");
  fDEMETRAShieldingCmd->SetGuidance("NoShield: no shielding selected");
  fDEMETRAShieldingCmd->SetDefaultValue("Full");
  fDEMETRAShieldingCmd->SetCandidates("Full NoShield");
  fDEMETRAShieldingCmd->SetParameterName("choice",false);
  fDEMETRAShieldingCmd->AvailableForStates(G4State_Init,G4State_Idle);
    
  fthick0Cmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/shield/thick0",this);
  fthick0Cmd->SetGuidance("Set thickness for shield layer #0; layers are numbered from 0 to 3, #0 being the outermost");
  fthick0Cmd->SetGuidance("This command is valid only if DEMETRAShielding has been selected as Full");
  fthick0Cmd->SetDefaultUnit("cm");
  fthick0Cmd->SetUnitCandidates("mm cm m");
  fthick0Cmd->SetParameterName("choice",false);
  fthick0Cmd->AvailableForStates(G4State_Init,G4State_Idle);
  
  fthick1Cmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/shield/thick1",this);
  fthick1Cmd->SetGuidance("Set thickness for shield layer #1; layers are numbered from 0 to 3, #0 being the outermost");
  fthick1Cmd->SetGuidance("This command is valid only if DEMETRAShielding has been selected as Full");
  fthick1Cmd->SetDefaultUnit("cm");
  fthick1Cmd->SetUnitCandidates("mm cm m");
  fthick1Cmd->SetParameterName("choice",false);
  fthick1Cmd->AvailableForStates(G4State_Init,G4State_Idle);
  
  fthick2Cmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/shield/thick2",this);
  fthick2Cmd->SetGuidance("Set thickness for shield layer #2; layers are numbered from 0 to 3, #0 being the outermost");
  fthick2Cmd->SetGuidance("This command is valid only if DEMETRAShielding has been selected as Full");
  fthick2Cmd->SetDefaultUnit("cm");
  fthick2Cmd->SetUnitCandidates("mm cm m");
  fthick2Cmd->SetParameterName("choice",false);
  fthick2Cmd->AvailableForStates(G4State_Init,G4State_Idle);
  
  fthick3Cmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/shield/thick3",this);
  fthick3Cmd->SetGuidance("Set thickness for shield layer #3; layers are numbered from 0 to 3, #0 being the outermost");
  fthick3Cmd->SetGuidance("This command is valid only if DEMETRAShielding has been selected as Full");
  fthick3Cmd->SetDefaultUnit("cm");
  fthick3Cmd->SetUnitCandidates("mm cm m");
  fthick3Cmd->SetParameterName("choice",false);
  fthick3Cmd->AvailableForStates(G4State_Init,G4State_Idle);
  
  fMat0Cmd = new G4UIcmdWithAString("/DEMETRA/shield/mat0",this);
  fMat0Cmd->SetGuidance("Set material of the DEMETRA shield layer #0. Possible candidates: Pb, PE, Cu, Air, Water");
  fMat0Cmd->SetGuidance("This command is valid only if DEMETRAShielding has been selected as Full");
  fMat0Cmd->SetParameterName("choice",true);
  fMat0Cmd->SetDefaultValue("Air");
  fMat0Cmd->SetCandidates("Pb PE Cu Air Water");
  fMat0Cmd->AvailableForStates(G4State_Init,G4State_Idle);
  
  fMat1Cmd = new G4UIcmdWithAString("/DEMETRA/shield/mat1",this);
  fMat1Cmd->SetGuidance("Set material of the DEMETRA shield layer #1. Possible candidates: Pb, PE, Cu, Air, Water");
  fMat1Cmd->SetGuidance("This command is valid only if DEMETRAShielding has been selected as Full");
  fMat1Cmd->SetParameterName("choice",true);
  fMat1Cmd->SetDefaultValue("Air");
  fMat1Cmd->SetCandidates("Pb PE Cu Air Water");
  fMat1Cmd->AvailableForStates(G4State_Init,G4State_Idle);
    
  fMat2Cmd = new G4UIcmdWithAString("/DEMETRA/shield/mat2",this);
  fMat2Cmd->SetGuidance("Set material of the DEMETRA shield layer #2. Possible candidates: Pb, PE, Cu, Air, Water");
  fMat2Cmd->SetGuidance("This command is valid only if DEMETRAShielding has been selected as Full");
  fMat2Cmd->SetParameterName("choice",true);
  fMat2Cmd->SetDefaultValue("Air");
  fMat2Cmd->SetCandidates("Pb PE Cu Air Water");
  fMat2Cmd->AvailableForStates(G4State_Init,G4State_Idle);
  
  fMat3Cmd = new G4UIcmdWithAString("/DEMETRA/shield/mat3",this);
  fMat3Cmd->SetGuidance("Set material of the DEMETRA shield layer #3. Possible candidates: Pb, PE, Cu, Air, Water");
  fMat3Cmd->SetGuidance("This command is valid only if DEMETRAShielding has been selected as Full");
  fMat3Cmd->SetParameterName("choice",true);
  fMat3Cmd->SetDefaultValue("Air");
  fMat3Cmd->SetCandidates("Pb PE Cu Air Water");
  fMat3Cmd->AvailableForStates(G4State_Init,G4State_Idle);
  
    // source not implemented at the moment but useful 

    /*
    fSourceCmd = new G4UIcmdWithAString("/DEMETRA/detector/calibration_source",this);
    fSourceCmd->SetGuidance("Set the calibration source: Possible choiches ON or OFF");
    fSourceCmd->SetGuidance("This option is available ONLY within -- FIXME ---");
    fSourceCmd->SetDefaultValue("OFF");
    fSourceCmd->SetParameterName("choice",true);
    fSourceCmd->AvailableForStates(G4State_Init,G4State_Idle);
    
    fSourcePosCmd = new G4UIcmdWithADoubleAndUnit("/DEMETRA/detector/calibration_source_position",this);
    fSourcePosCmd->SetGuidance("Set the position of the source respect to the enclosure center");
    fSourcePosCmd->SetGuidance("Possible value is any number between -29 cm (enclosure bottom) and 1182 cm (Top of the CIS copper tube");
    fSourcePosCmd->SetParameterName("choice",true);
    fSourcePosCmd->SetRange("choice > -28.75 && choice < 1181.75");
    fSourcePosCmd->SetDefaultValue(0);
    fSourcePosCmd->SetDefaultUnit("cm");
    */
    
  fupdateCmd = new G4UIcmdWithoutParameter("/DEMETRA/updateGeo", this);
  fupdateCmd->SetGuidance("Update detector geometry after settings");
}

DEMETRADetectorConstructionMessenger::~DEMETRADetectorConstructionMessenger()
{
  delete fDEMETRASetupCmd;
    
  //    delete fNumXCmd;
  //    delete fNumYCmd;
  //    delete fNumZCmd;

  delete fDetXCmd;
  delete fDetYCmd;
  delete fDetZCmd;

  delete fAirInnerRadiusCmd;
  delete fAirThicknessCmd;
  delete fexternalrockthickCmd;
  delete fproductionrockthickCmd;
  delete finternalrockthickCmd;
  
  delete fthick0Cmd;
  delete fthick1Cmd;
  delete fthick2Cmd;
  delete fthick3Cmd;
  delete fMat0Cmd;
  delete fMat1Cmd;
  delete fMat2Cmd;
  delete fMat3Cmd;
  
  //    delete fSourceCmd;
  //    delete fSourcePosCmd;
  
  delete fupdateCmd;
}

//---------------------------------------------------------------------------//

void DEMETRADetectorConstructionMessenger::SetNewValue(G4UIcommand *command,
                                                     G4String newValue)
{
  if (command == fDEMETRASetupCmd )
    {
      fDetectorPrimary->SetDEMETRASetup(newValue);
      //fDetectorPrimary->UpdateGeometry();
    }
  /*
    else if (command == fNumXCmd )
    {
    fDetectorPrimary->SetNumX(fNumXCmd->GetNewIntValue(newValue));
    //fDetectorPrimary->UpdateGeometry();
    }
    else if (command == fNumYCmd )
    {
    fDetectorPrimary->SetNumY(fNumYCmd->GetNewIntValue(newValue));
    //fDetectorPrimary->UpdateGeometry();
    }
    else if (command == fNumZCmd )
    {
    fDetectorPrimary->SetNumZ(fNumZCmd->GetNewIntValue(newValue));
    //fDetectorPrimary->UpdateGeometry();
    }
  */
  else if (command == fDEMETRALabCmd )
    {
      fDetectorPrimary->SetDEMETRALab(newValue);
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fAirInnerRadiusCmd )
    {
      fDetectorPrimary->SetAirInnerRadius(fAirInnerRadiusCmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fAirThicknessCmd )
    {
      fDetectorPrimary->SetAirThickness(fAirThicknessCmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fexternalrockthickCmd )
    {
      fDetectorPrimary->SetExternalRockThickness(fexternalrockthickCmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fproductionrockthickCmd )
    {
      fDetectorPrimary->SetProductionRockThickness(fproductionrockthickCmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == finternalrockthickCmd )
    {
      fDetectorPrimary->SetInternalRockThickness(finternalrockthickCmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  if (command == fDEMETRAShieldingCmd )
    {
      fDetectorPrimary->SetDEMETRAShielding(newValue);
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fDetXCmd )
    {
      fDetectorPrimary->SetDetX(fDetXCmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fDetYCmd )
    {
      fDetectorPrimary->SetDetY(fDetYCmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fDetZCmd )
    {
      fDetectorPrimary->SetDetZ(fDetZCmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fthick0Cmd )
    {
      fDetectorPrimary->SetShieldThick0(fthick0Cmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fthick1Cmd )
    {
      fDetectorPrimary->SetShieldThick1(fthick1Cmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fthick2Cmd )
    {
      fDetectorPrimary->SetShieldThick2(fthick2Cmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fthick3Cmd )
    {
      fDetectorPrimary->SetShieldThick3(fthick3Cmd->GetNewDoubleValue(newValue));
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fMat0Cmd )
    {
      fDetectorPrimary->SetShield0Material(newValue);
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fMat1Cmd )
    {
      fDetectorPrimary->SetShield1Material(newValue);
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fMat2Cmd )
    {
      fDetectorPrimary->SetShield2Material(newValue);
      //fDetectorPrimary->UpdateGeometry();
    }
  else if (command == fMat3Cmd )
    {
      fDetectorPrimary->SetShield3Material(newValue);
      //fDetectorPrimary->UpdateGeometry();
    }
  else if ( command == fupdateCmd )
    {
      fDetectorPrimary->UpdateGeometry();
    }
  /*
    if (command == fSourceCmd){
    fDetectorPrimary->SetCalibSource(newValue);
    }
    else if(command == fSourcePosCmd){
    fDetectorPrimary->SetCalibSourcePosition(fSourcePosCmd->GetNewDoubleValue(newValue));
    }
  */
}
