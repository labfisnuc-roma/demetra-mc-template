#include "DEMETRAHit.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4VisAttributes.hh"

G4ThreadLocal G4Allocator<DEMETRAHit>* DEMETRAHitAllocator=0;

DEMETRAHit::DEMETRAHit() {}

DEMETRAHit::~DEMETRAHit() {}

DEMETRAHit::DEMETRAHit(const DEMETRAHit& right)
  : G4VHit()
{
  parentID   = right.parentID;
  particleID   = right.particleID;
  trackID   = right.trackID;
  globalTime = right.globalTime;
  kinEne = right.kinEne;
  processIni = right.processIni;
  processFin = right.processFin;
  detN      = right.detN;
  edep      = right.edep;
  pos       = right.pos;
}

const DEMETRAHit& DEMETRAHit::operator=(const DEMETRAHit& right)
{
  parentID   = right.parentID;
  particleID  = right.particleID;
  trackID   = right.trackID;
  globalTime = right.globalTime;
  kinEne = right.kinEne;
  processIni = right.processIni;
  processFin = right.processFin;
  detN      = right.detN;
  edep      = right.edep;
  pos       = right.pos;

  return *this;
}

G4int DEMETRAHit::operator==(const DEMETRAHit& right) const
{
  return (this==&right) ? 1 : 0;
}

void DEMETRAHit::Print()
{
  G4cout << "  parent id : " << parentID
	 << "  particleID : " << particleID
	 << "  track id : " << trackID
 	 << "  global time : " << G4BestUnit(globalTime,"Time")
	 << "  kinetic energy : " << G4BestUnit(kinEne,"Energy")
	 << "  process ini : " << processIni 
	 << "  process fin : " << processFin 
     << "  detector number : " << detN
	 << "  energy deposit : " << G4BestUnit(edep,"Energy")
	 << "  position : " << G4BestUnit(pos,"Length") << G4endl;
}

