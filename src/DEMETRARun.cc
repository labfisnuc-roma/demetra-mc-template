#include "DEMETRARun.hh"
#include "G4Event.hh"
#include "G4SDManager.hh"
#include "DEMETRAHit.hh"
#include "G4HCofThisEvent.hh"
#include "DEMETRAAnalysis.hh"

DEMETRARun::DEMETRARun() :
    G4Run(),
    em_ene(0),
    had_ene(0),
    shower_shape(0),
    DEMETRAID(-1)
{ }


void DEMETRARun::RecordEvent(const G4Event* evt)
{

    //Forward call to base class
    G4Run::RecordEvent(evt);

    if ( DEMETRAID == -1) {
      G4SDManager * SDman = G4SDManager::GetSDMpointer();
      DEMETRAID = SDman->GetCollectionID("DEMETRACollection");
    } 
    G4HCofThisEvent* HCE = evt->GetHCofThisEvent();
    if (!HCE) {
      G4ExceptionDescription msg;
      msg << "No hits collection of this event found.\n";
      G4Exception("Run::RecordEvent()",
		  "Code001", JustWarning, msg);
      return;
    }


  DEMETRAHitsCollection* DEMETRAHC = 0;
  if (DEMETRAID != -1) DEMETRAHC = static_cast<DEMETRAHitsCollection*>(HCE->GetHC(DEMETRAID));
  
  //this is just in case we want to do something with the event in multiple runs

  DEMETRAAnalysis* analysis = DEMETRAAnalysis::getInstance();
  analysis->EndOfEvent(evt);
}

void DEMETRARun::Merge(const G4Run* aRun)
{
    const DEMETRARun* localRun = static_cast<const DEMETRARun*>(aRun);
    //em_ene += localRun->GetEmEnergy();
    //had_ene += localRun->GetHadEnergy();
    //shower_shape += localRun->GetShowerShape();
    //Forward call to base-class
    //G4Run::Merge(localRun);
}
