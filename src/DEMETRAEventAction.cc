#include "DEMETRAEventAction.hh"
#include "DEMETRARunAction.hh"
#include "DEMETRAEventActionMessenger.hh"
#include "DEMETRAAnalysis.hh"
#include "DEMETRADetectorConstruction.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4ios.hh"


DEMETRAEventAction::DEMETRAEventAction(DEMETRARunAction* run, DEMETRADetectorConstruction* myDC)
  : G4UserEventAction(), fRunAct(run), fDetector(myDC)
{
  fMessenger = new DEMETRAEventActionMessenger(this);
  fDetectorHit = false;
  repfreq = 1000;
}

DEMETRAEventAction::~DEMETRAEventAction()
{
  delete fMessenger;
}

void DEMETRAEventAction::BeginOfEventAction(const G4Event* evt)
{

  DEMETRAAnalysis* analysis = DEMETRAAnalysis::getInstance();
  analysis->BeginOfEvent(evt,fDetector);
  
}

void DEMETRAEventAction::EndOfEventAction(const G4Event* evt)
{
  G4int event_id = evt->GetEventID();
  
  // get number of stored trajectories
  //
  G4TrajectoryContainer* trajectoryContainer = evt->GetTrajectoryContainer();
  G4int n_trajectories = 0;
  if (trajectoryContainer) n_trajectories = trajectoryContainer->entries();
  
  if (event_id%repfreq == 0) 
    {
      G4cout << ">>> Event " << evt->GetEventID() << G4endl;
      G4cout << "    " << n_trajectories << " trajectories stored in this event." << G4endl;
    }

}

