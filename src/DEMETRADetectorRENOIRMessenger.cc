#include "globals.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIdirectory.hh"
#include "G4PhysicalVolumeStore.hh"

#include "DEMETRADetectorRENOIR.hh"
#include "DEMETRADetectorRENOIRMessenger.hh"

//---------------------------------------------------------------------------//

DEMETRADetectorRENOIRMessenger::DEMETRADetectorRENOIRMessenger
(DEMETRADetectorRENOIR *detector):fDetectorPrimary(detector)
{
  fDetectorDirectory = new G4UIdirectory("/RENOIR/detector/");
  fDetectorDirectory->SetGuidance("Control commands for detector RENOIR:");

  // source not implemented at the moment but useful 

  /*
    fSourceCmd = new G4UIcmdWithAString("/RENOIR/detector/calibration_source",this);
    fSourceCmd->SetGuidance("Set the calibration source: Possible choiches ON or OFF");
    fSourceCmd->SetGuidance("This option is available ONLY within -- FIXME ---");
    fSourceCmd->SetDefaultValue("OFF");
    fSourceCmd->SetParameterName("choice",true);
    fSourceCmd->AvailableForStates(G4State_Init,G4State_Idle);
    
    fSourcePosCmd = new G4UIcmdWithADoubleAndUnit("/RENOIR/detector/calibration_source_position",this);
    fSourcePosCmd->SetGuidance("Set the position of the source respect to the enclosure center");
    fSourcePosCmd->SetGuidance("Possible value is any number between -29 cm (enclosure bottom) and 1182 cm (Top of the CIS copper tube");
    fSourcePosCmd->SetParameterName("choice",true);
    fSourcePosCmd->SetRange("choice > -28.75 && choice < 1181.75");
    fSourcePosCmd->SetDefaultValue(0);
    fSourcePosCmd->SetDefaultUnit("cm");
  */
  
  fShieldDirectory = new G4UIdirectory("/RENOIR/shield/");
  fShieldDirectory->SetGuidance("Control commands for inner shield of the RENOIR detector:");

  /*
    fRENOIRShieldingCmd = new G4UIcmdWithAString("/RENOIR/shield/select",this);
    fRENOIRShieldingCmd->SetGuidance("Select the RENOIR shielding geometry. Possible choices: Full, NoShield");
    fRENOIRShieldingCmd->SetGuidance("Full: four boxes of different thicknesses and materials. Shielding thickness and material to be selected by user");
    fRENOIRShieldingCmd->SetGuidance("NoShield: no shielding selected");
    fRENOIRShieldingCmd->SetDefaultValue("Full");
    fRENOIRShieldingCmd->SetCandidates("Full NoShield");
    fRENOIRShieldingCmd->SetParameterName("choice",false);
    fRENOIRShieldingCmd->AvailableForStates(G4State_Init,G4State_Idle);
  */
  
}

DEMETRADetectorRENOIRMessenger::~DEMETRADetectorRENOIRMessenger()
{
  
  //    delete fSourceCmd;
  //    delete fSourcePosCmd;

}

//---------------------------------------------------------------------------//

void DEMETRADetectorRENOIRMessenger::SetNewValue(G4UIcommand *command,
                                                     G4String newValue)
{
  /*
  if (command == fDEMETRAShieldingCmd )
    {
      fDetectorPrimary->SetDEMETRAShielding(newValue);
      //fDetectorPrimary->UpdateGeometry();
    }

    else if (command == fSourceCmd)
    {  
      fDetectorPrimary->SetCalibSource(newValue);
    }
   else if(command == fSourcePosCmd){
   fDetectorPrimary->SetCalibSourcePosition(fSourcePosCmd->GetNewDoubleValue(newValue));
   }
  */
}
