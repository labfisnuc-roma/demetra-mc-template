/* ************************************************
 * INFN RM1 GEANT4 SIMULATION GROUP 
 *
 * File:      DetectorConstruction.cc
 *
 **************************************************/

// CADMESH //
//#include "CADMesh.hh"

// GEANT4 //
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
//#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Polyhedra.hh"
#include "G4Sphere.hh"
#include "G4EllipticalTube.hh"
#include "G4Ellipsoid.hh"
#include "G4IntersectionSolid.hh"

#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4AssemblyVolume.hh"

#include "G4GeometryManager.hh"
#include "G4RunManager.hh"
#include "G4Region.hh"
#include "G4RegionStore.hh"
#include "G4ProductionCuts.hh"

#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4NistManager.hh"
#include "G4Material.hh"
#include "G4VisAttributes.hh"

#include "G4SDManager.hh"

// USER //
#include "DEMETRADetectorConstruction.hh"
#include "DEMETRADetectorConstructionMessenger.hh"
#include "DEMETRADetectorLNGS.hh"
#include "DEMETRADetectorRENOIR.hh"
#include "DEMETRADetectorMaterial.hh"
#include "DEMETRADetectorProperty.hh"
#include "DEMETRASensitiveDetector.hh"

DEMETRADetectorConstruction::DEMETRADetectorConstruction() :
   rockThicknessOuter(-999*m),
   rockThicknessInner(-999*m),
   DEMETRASetup("DEMETRA"),
   DEMETRALab("NoCave"),
   DEMETRAShielding("Full"),
   Shield0MatStr("Air"),
   Shield1MatStr("Air"),
   Shield2MatStr("Air"),
   Shield3MatStr("Air"),
   thick0(10.0*cm),
   thick1(10.0*cm),
   thick2(10.0*cm),
   thick3(10.0*cm),
   airInnerRadius(10.0*cm),
   airThickness(5.0*cm),
   fDetX(1.0*cm),
   fDetY(1.0*cm),
   fDetZ(1.0*cm),
   Detector_log(0),
   fDetectorRegion(0),
   fShieldRegion(0),
   fDetectorCuts(0),
   fShieldCuts(0)


{ 
 fMessenger = new DEMETRADetectorConstructionMessenger(this);
 fDetectorCuts = new G4ProductionCuts();
 fShieldCuts   = new G4ProductionCuts();

}

DEMETRADetectorConstruction::~DEMETRADetectorConstruction()
{
 delete fMessenger;
 delete fDetectorCuts;
 delete fShieldCuts;

}

G4VPhysicalVolume* DEMETRADetectorConstruction::Construct()
{
    G4NistManager * nist_manager = G4NistManager::Instance();

    // Regions for production cuts ///
    // ----------------------------//
    if(fDetectorRegion) {
      delete fDetectorRegion;
    }
    if(fShieldRegion) {
      delete fShieldRegion;
    }
    fDetectorRegion = new G4Region("Detector");
    fDetectorRegion->SetProductionCuts(fDetectorCuts);

    fShieldRegion   = new G4Region("Shield");
    fShieldRegion->SetProductionCuts(fShieldCuts);

    
    //-----------------------------
    // construction of materials
    //-----------------------------
    G4cout << "Constructing materials...";

    //G4Material * air = nist_manager->FindOrBuildMaterial("G4_AIR");
    //G4Material * water = nist_manager->FindOrBuildMaterial("G4_WATER");
    DEMETRADetectorMaterial* DEMETRAMaterials = DEMETRADetectorMaterial::GetInstance();
    G4cout << "... done" << G4endl;

    //**********************************************************************
    //   DEFINITION OF THE GEOMETRY
    //**********************************************************************
      
    //INITIALIZING TRANSLATION VECTORS TO 0:
    Rock_log = 0;
    size_Rock = G4ThreeVector();
    tr_Rock = G4ThreeVector();
    rot_Rock = G4RotationMatrix();
    absrot_Rock = G4RotationMatrix();
    Laboratory_log = 0;
    size_Laboratory = G4ThreeVector();
    tr_Laboratory = G4ThreeVector();
    rot_Laboratory = G4RotationMatrix();
    Shielding_log = 0;
    size_Shielding = G4ThreeVector();
    tr_Shielding = G4ThreeVector();
    rot_Shielding = G4RotationMatrix();
    absrot_Shielding = G4RotationMatrix();
    InsideVolume_log = 0;
    size_InsideVolume = G4ThreeVector();
    tr_InsideVolume = G4ThreeVector();
    rot_InsideVolume = G4RotationMatrix();
    Detector_log = 0;

    //Name of the volumes
    G4String name_solid="";
    G4String name_log="";
    G4String name_phys="";

    G4ThreeVector tr;
    G4RotationMatrix rot;

    G4double world_x = 100.0*m;
    G4double world_y = 100.0*m;
    G4double world_z = 400.0*m;
      
    //**********************************************************************
    // WORLD ***************************************
    //**********************************************************************
    name_phys="WorldVolume";
    name_log=name_phys+"_log";
    name_solid=name_phys+"_solid";
    G4Box* WorldVolume_box = new G4Box(name_solid,0.5*world_x,0.5*world_y,0.5*world_z);
    WorldVolume_log = new G4LogicalVolume(WorldVolume_box,DEMETRAMaterials->Material("Vacuum"),name_log,0,0,0);
    //THE WORLD CANNOT BE TRANSLATED, therefore the first volume inside it (Rock_log) is the volume that must be translated in order to adjust the origin. Rock_log cannot be as large as the world to avoid that this volume is moved out of the world
    WorldVolume_phys = new G4PVPlacement(0,G4ThreeVector(),WorldVolume_log,name_phys,0,false,0,true);//The world volume cannot be translated
          
    //These variables are used to set the thin tube for depth studies in the externalRock_log
    G4double rockdist_z;
    G4double rockdepth_z;
        
    //**********************************************************************
    // LABORATORY ***************************************
    //**********************************************************************
    G4cout << "Constructing laboratory..." << G4endl;
    G4bool isThinTubeCompatible=false;
    // ---------------------------------- LNGS
    if (DEMETRALab == "LNGS"){
    
        DEMETRADetectorLNGS* LNGS = DEMETRADetectorLNGS::GetInstance();
        if (rockThicknessOuter != -999*m)
              LNGS->SetExternalRockThickness(rockThicknessOuter);
        if (productionLayerThickness != -999*m)
              LNGS->SetProductionRockThickness(productionLayerThickness);
        if (rockThicknessInner != -999*m)
              LNGS->SetInternalRockThickness(rockThicknessInner);	  
        LNGS->ConstructRock();
        Rock_log=LNGS->GetRock();
        size_Rock=LNGS->GetRockSizeXYZ();
        absrot_Rock=LNGS->GetRockAbsRotation();
        Laboratory_log=LNGS->GetLaboratory();
        size_Laboratory=LNGS->GetLaboratorySizeXYZ();
        tr_Laboratory=LNGS->GetLaboratoryTranslation();
        rot_Laboratory=LNGS->GetLaboratoryRotation();
        
        ////for thin tube
        //if(LNGS->GetProductionRockThickness()==0.*cm && LNGS->GetInternalRockThickness()==0.*cm)
        //  isThinTubeCompatible=true;
        //rockdist_z=LNGS->rockdist_z;
        //rockdepth_z=LNGS->rockdepth_z;
    }
    // ---------------------------------- NoCave
    else if (DEMETRALab == "NoCave") 
	{
	  //**********************************************************************
	  // Double Air sphere surrounding the whole detector and shielding
	  //**********************************************************************        
	  airInnerRadius = 10.0*m;
	  airThickness = 5.0*m;
	  if (productionLayerThickness != -999*m)
		airThickness = productionLayerThickness;
	  if (rockThicknessInner != -999*m)
		airInnerRadius=rockThicknessInner;

	  //Air permeates the are around the detector
	  name_phys="OuterAirSphere";
	  name_log=name_phys+"_log";
	  name_solid=name_phys+"_solid";
	  G4Sphere* OuterAirSphere = new G4Sphere(name_solid, 0., airInnerRadius+airThickness, 0*degree, 360*degree, 0*degree,180*degree);
	  G4LogicalVolume* OuterAirSphere_log = new G4LogicalVolume(OuterAirSphere,DEMETRAMaterials->Material("Air"),name_log);
	  Rock_log=OuterAirSphere_log;
	  size_Rock=G4ThreeVector(airInnerRadius+airThickness,airInnerRadius+airThickness,airInnerRadius+airThickness);
	  absrot_Rock = G4RotationMatrix();
		
	  name_phys="InnerAirSphere";
	  name_log=name_phys+"_log";
	  name_solid=name_phys+"_solid";
	  G4Sphere* InnerAirSphere = new G4Sphere(name_solid,  0., airInnerRadius,  0*degree, 360*degree, 0*degree,180*degree);
	  G4LogicalVolume* InnerAirSphere_log = new G4LogicalVolume(InnerAirSphere,
																DEMETRAMaterials->Material("Air"),
																name_log);
	  Laboratory_log=InnerAirSphere_log;
	  size_Laboratory=G4ThreeVector(airInnerRadius,airInnerRadius,airInnerRadius);
	  tr = G4ThreeVector(0.,0.,0.);//translation in mother frame
	  tr_Laboratory+=(rot_Laboratory*tr);
	  rot = G4RotationMatrix();// rotation of daughter volume
	  rot_Laboratory*=rot; //equivalent to rot_Laboratory=rot_Laboratory*rot
	  G4PVPlacement* InnerAirSphere_phys = new G4PVPlacement(G4Transform3D(rot,tr),InnerAirSphere_log,name_phys,OuterAirSphere_log,false,0,true);        
    }

    G4cout << "Laboratory done." << G4endl;
  
    //**********************************************************************
    // ********* DEMETRA customizable external shielding *******************
    //**********************************************************************

    // ----------------------------------- Inner room dimensions

        G4double AirBox_x = 2.*m;
        G4double AirBox_y = 2.*m;
        G4double AirBox_z = 2.*m;
        
        name_phys="AirBox";
        name_log=name_phys+"_log";
        name_solid=name_phys+"_solid";

        G4Box* AirBox = new G4Box(name_solid,0.5*AirBox_x,0.5*AirBox_y,0.5*AirBox_z);
        G4LogicalVolume* AirBox_log = new G4LogicalVolume(AirBox,DEMETRAMaterials->Material("Air"),name_log,0,0,0);
        AirBox_log->SetVisAttributes(G4Color(1.,0.,0.));
        
        // ----------------------------------- Shield 0
        // dimensions are declared and initialized above --> Shield0_x, Shield0_y, Shield0_z
        
        G4double Shield0_x = AirBox_x + 2.*thick3 + 2.*thick2 + 2.*thick1 + 2.*thick0 ;
        G4double Shield0_y = AirBox_y + 2.*thick3 + 2.*thick2 + 2.*thick1 + 2.*thick0 ;
        G4double Shield0_z = AirBox_z + 2.*thick3 + 2.*thick2 + 2.*thick1 + 2.*thick0 ;
        
        G4Material* Shield0Mat = DEMETRAMaterials->Material(Shield0MatStr);

        name_phys="Shield0";
        name_log=name_phys+"_log";
        name_solid=name_phys+"_solid";
        G4Box* Shield0 = new G4Box(name_solid,0.5*Shield0_x,0.5*Shield0_y,0.5*Shield0_z);
        G4LogicalVolume* Shield0_log = new G4LogicalVolume(Shield0,Shield0Mat,name_log,0,0,0);
        
        // ----------------------------------- Shield 1
        
        G4double Shield1_x = AirBox_x + 2.*thick3 + 2.*thick2 + 2.*thick1 ;
        G4double Shield1_y = AirBox_y + 2.*thick3 + 2.*thick2 + 2.*thick1 ;
        G4double Shield1_z = AirBox_z + 2.*thick3 + 2.*thick2 + 2.*thick1 ;
        
        G4Material* Shield1Mat = DEMETRAMaterials->Material(Shield1MatStr);
        
        name_phys="Shield1";
        name_log=name_phys+"_log";
        name_solid=name_phys+"_solid";
        G4Box* Shield1 = new G4Box(name_solid,0.5*Shield1_x,0.5*Shield1_y,0.5*Shield1_z);
        G4LogicalVolume* Shield1_log = new G4LogicalVolume(Shield1,Shield1Mat,name_log,0,0,0);
  
        // ----------------------------------- Shield 2
        
        G4double Shield2_x = AirBox_x + 2.*thick3 + 2.*thick2 ;
        G4double Shield2_y = AirBox_y + 2.*thick3 + 2.*thick2 ;
        G4double Shield2_z = AirBox_z + 2.*thick3 + 2.*thick2 ;
        
        G4Material* Shield2Mat = DEMETRAMaterials->Material(Shield2MatStr);
        
        name_phys="Shield2";
        name_log=name_phys+"_log";
        name_solid=name_phys+"_solid";
        G4Box* Shield2 = new G4Box(name_solid,0.5*Shield2_x,0.5*Shield2_y,0.5*Shield2_z);
        G4LogicalVolume* Shield2_log = new G4LogicalVolume(Shield2,Shield2Mat,name_log,0,0,0);
        
        // ----------------------------------- Shield 3
        
        G4double Shield3_x = AirBox_x + 2.*thick3 ;
        G4double Shield3_y = AirBox_y + 2.*thick3 ;
        G4double Shield3_z = AirBox_z + 2.*thick3 ;
        
        G4Material* Shield3Mat = DEMETRAMaterials->Material(Shield3MatStr);
        
        name_phys="Shield3";
        name_log=name_phys+"_log";
        name_solid=name_phys+"_solid";
        G4Box* Shield3 = new G4Box(name_solid,0.5*Shield3_x,0.5*Shield3_y,0.5*Shield3_z);
        G4LogicalVolume* Shield3_log = new G4LogicalVolume(Shield3,Shield3Mat,name_log,0,0,0);

        Shield0_phys = new G4PVPlacement(0,G4ThreeVector(),Shield0_log,"Shield0",Laboratory_log,false,0,true);
        Shield1_phys = new G4PVPlacement(0,G4ThreeVector(),Shield1_log,"Shield1",Shield0_log,false,0,true);
        Shield2_phys = new G4PVPlacement(0,G4ThreeVector(),Shield2_log,"Shield2",Shield1_log,false,0,true);
        Shield3_phys = new G4PVPlacement(0,G4ThreeVector(),Shield3_log,"Shield3",Shield2_log,false,0,true);

	AirBox_phys = new G4PVPlacement(0,G4ThreeVector(), AirBox_log, "AirBox", Shield3_log, false, 0,true);

	//**********************************************************************
	//********* DEMETRA detector *****************************************
	//**********************************************************************

	G4double det_x = fDetX;
	G4double det_y = fDetY;
	G4double det_z = fDetZ;

	//        G4double det_x = 10.*mm;
	//        G4double det_y = 10.*mm;
	//        G4double det_z = 0.3*mm;
        
        name_phys="Detector";
        name_log=name_phys+"_log";
        name_solid=name_phys+"_solid";
	
	if (DEMETRASetup == "DEMETRA") {
	  G4Box* Detector = new G4Box(name_solid,0.5*det_x,0.5*det_y,0.5*det_z);
	  Detector_log = new G4LogicalVolume(Detector,DEMETRAMaterials->Material("Si"),name_log,0,0,0);  G4String name_log=name_phys+"_log";
	  Detector_phys = new G4PVPlacement(0,G4ThreeVector(),Detector_log,"Detector",AirBox_log,false,0,true);
	}

	else if (DEMETRASetup == "RENOIR") { 

	  G4ThreeVector tr_assembly;
	  tr_assembly = G4ThreeVector(0.,0.,0.); //translation for the assembly volume
	  G4RotationMatrix* rot_assembly;
	  rot_assembly = new G4RotationMatrix();

	  DEMETRADetectorRENOIR* RENOIR = DEMETRADetectorRENOIR::GetInstance();
	  Detector_log = RENOIR->GetDetector();
	  G4AssemblyVolume* assemblyRENOIR = RENOIR->GetAssembly();
	  G4LogicalVolume* shieldRENOIR = RENOIR->GetPbShield();
	  assemblyRENOIR->MakeImprint(AirBox_log,tr_assembly,rot_assembly);
	  PbShield_phys = new G4PVPlacement(0,G4ThreeVector(),shieldRENOIR,"PbShield",AirBox_log,false,0,true);
	}

	//**********************************************************************
	//********* DEMETRA detector array (not yet implemented) ***************
	//**********************************************************************

	NumX = 1.;
	NumY = 1.;
	NumZ = 1.;

    //**********************************************************************
    // ********* DEMETRA volumes form CADMesh *****************************
    //**********************************************************************
    /*
    CADMesh * mesh_shell = new CADMesh("/storage/giulia/DEMETRA/geometry/v1/shell.stl");    
    CADMesh * mesh_camera_carter = new CADMesh("/storage/giulia/DEMETRA/geometry/v1/carters.stl");    
    CADMesh * mesh_camera = new CADMesh("/storage/giulia/DEMETRA/geometry/v1/cameras_all.stl");    

    mesh_shell->SetScale(mm);
    mesh_camera_carter->SetScale(mm);
    mesh_camera->SetScale(mm);


    //shell
    cad_shell_solid = mesh_shell->TessellatedMesh();
    cad_shell_logical = new G4LogicalVolume(cad_shell_solid, DEMETRAMaterials->Material("Perspex"), "cad_shell_logical", 0, 0, 0);
    
    //camera carter
    cad_camera_carter_solid = mesh_camera_carter->TessellatedMesh();
    cad_camera_carter_logical = new G4LogicalVolume(cad_camera_carter_solid, DEMETRAMaterials->Material("Perspex"), "cad_camera_carter_logical", 0, 0, 0);
    
    //cameras
    cad_cameras_all_solid = mesh_camera->TessellatedMesh();
    cad_cameras_all_logical = new G4LogicalVolume(cad_cameras_all_solid, DEMETRAMaterials->Material("Perspex"), "cad_cameras_all_logical", 0, 0, 0);
    
    //glass window
    
    //DEMETRA gas
    
    //GEM support
    
    //cathode
    
    //fili
    
    //angolari	 
    
    //GEM

    */

    if (DEMETRALab == "LNGS"){
    	tr_cad+=G4ThreeVector(0.,-1*size_Laboratory.y()+1*m,size_Laboratory.z()-10*m);	  
    }
    else if (DEMETRALab == "NoCave") {}
    G4ThreeVector  size;
    rot_cad=(rot_Laboratory.inverse()*absrot_Rock.inverse())*absrot_cad;//Rotation of the DEMETRA outer volume
    //cad_physical = new G4PVPlacement(0, G4ThreeVector(), cad_logical,
    //    cad_shell_physical = new G4PVPlacement(G4Transform3D(rot_cad,tr_cad), 
    //		    cad_shell_logical,"cad_shell_physical", Laboratory_log, false, 0);
//    cad_camera_carter_physical = new G4PVPlacement(G4Transform3D(rot_cad,tr_cad), 
//		    cad_camera_carter_logical,"cad_camera_carter_physical", Laboratory_log, false, 0);
//    cad_cameras_all_physical = new G4PVPlacement(G4Transform3D(rot_cad,tr_cad), 
//		    cad_cameras_all_logical,"cad_cameras_all_physical", Laboratory_log, false, 0);

    

    //
    //**********************************************************************
    // GLOBAL TRANSLATIONS ***************************************
    G4cout<<"Placement of Laboratory in the World started"<<G4endl;
    
//    tr_Rock=-1*(tr_Laboratory+rot_Laboratory*(tr_Shielding+rot_Shielding*(tr_InsideVolume+rot_InsideVolume*(tr_Vessel+rot_Vessel*(tr_Scintillator+rot_Scintillator*(tr_Assembly+rot_Assembly*(rot_Enclosure*(tr_Crystal))))))));//The shift of Rock_log in the world volume to make the origin be the center of the crystal in the middle of the Assembly volume
    tr_Rock=-1*(tr_Laboratory+rot_Laboratory*(tr_cad+rot_cad*(tr_InsideVolume+rot_InsideVolume*(tr_cad))));//The shift of Rock_log in the world volume to make the origin be the center of the detector

    //G4RotationMatrix rot_check = absrot_Rock*(rot_Laboratory*(rot_Shielding*(rot_InsideVolume*(rot_Vessel*(rot_Scintillator*(rot_Assembly*rot_Enclosure))))));  
    G4RotationMatrix rot_check = absrot_Rock*(rot_Laboratory*(rot_cad*(rot_InsideVolume)));  
    //if (rot_check != absrot_Enclosure)
    //      {
    //        G4cout << "ERROR: the product of all the rotations does not coincide with the desired absolute rotation of the enclosures" << G4endl;
    //        //throw std::exception();
    //        exit(1);		
    //      }

    //Translating the rock in order to have the origin of the coordinate system in the middle of the crystal
    name_phys="externalRock";
    name_log=name_phys+"_log";
    name_solid=name_phys+"_solid";
    G4PVPlacement* externalRock_phys = new G4PVPlacement(G4Transform3D(absrot_Rock,tr_Rock),Rock_log,name_phys,WorldVolume_log,false,0,true);

    G4cout << "The shielding selected is: " << DEMETRAShielding << G4endl;
    G4cout << "thick0 is: " << thick0 << G4endl;
    G4cout << "thick1 is: " << thick1 << G4endl;
    G4cout << "thick2 is: " << thick2 << G4endl;
    G4cout << "thick3 is: " << thick3 << G4endl;
    G4cout << "The main DEMETRA volume is translated w.r.t the center of the rock volume of:\t x="<< -1*tr_Rock.x()/cm << " cm\t y=" << -1*tr_Rock.y()/cm << " cm\t z=" << -1*tr_Rock.z()/cm << " cm"<< G4endl;
    G4cout << "The Rock volume has been translated to put the main DEMETRA volume in the center of the coordinate system"<< G4endl;
    G4cout<<"Placement of Laboratory in the World ended"<<G4endl;
    
   
    //**********************************************************************
    // ********* DEMETRA sensitive volume **********************************
    //**********************************************************************

    //register the SD
    G4SDManager *SDmanager=G4SDManager::GetSDMpointer();
    
    // create a SD representing DEMETRA detector
    
    G4String DEMETRASDname = "DEMETRA/DEMETRASD";
    DEMETRASD = new DEMETRASensitiveDetector( DEMETRASDname );
    SDmanager->AddNewDetector( DEMETRASD );
   
    Detector_log->SetSensitiveDetector(DEMETRASD);

    // ---------------------------------------
    // Set regions for production cuts
    // ---------------------------------------

    // Define region for the sensitive detector
    fDetectorRegion->AddRootLogicalVolume(Detector_log);
  
    // Define region for the shield
    fShieldRegion->AddRootLogicalVolume(Shield0_log);
    fShieldRegion->AddRootLogicalVolume(Shield1_log);
    fShieldRegion->AddRootLogicalVolume(Shield2_log);
    fShieldRegion->AddRootLogicalVolume(Shield3_log);



    return WorldVolume_phys;
}

void DEMETRADetectorConstruction::UpdateGeometry()
{

  G4cout << "Updating the Geometry"<< G4endl;

  DEMETRADetectorProperty* DEMETRAProperties = DEMETRADetectorProperty::GetInstance();
  DEMETRAProperties->Refresh();

  //Removing sensitive detectors
  Detector_log->SetSensitiveDetector(0);
  Detector_log=0;

  //Deleting all the solids, logical and physical objects

  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore* PhysicalVolumeStore = G4PhysicalVolumeStore::GetInstance();
  PhysicalVolumeStore->Clean();
  G4LogicalVolumeStore* LogicalVolumeStore = G4LogicalVolumeStore::GetInstance();
  LogicalVolumeStore->Clean();
  G4SolidStore* SolidStore = G4SolidStore::GetInstance();
  SolidStore->Clean();
  
  //The memory for these pointers has been freed by the above Clean() methods
  //  DEMETRADetectorModule* DEMETRAModules = DEMETRADetectorModule::GetInstance();
  //  DEMETRAModules->Refresh();

  Shielding_log=0;
  Laboratory_log=0;
  Rock_log=0;
  WorldVolume_log=0;
  productionRockThinTube_phys=0;

  //  DEMETRADetectorMaterial* DEMETRAMaterials = DEMETRADetectorMaterial::GetInstance();
  //  DEMETRAMaterials->Refresh();

  // Delete all the geometry you had defined and build everything from scratch
  G4RunManager::GetRunManager()->DefineWorldVolume(Construct());
  G4RunManager::GetRunManager()->GeometryHasBeenModified();
  
}
