#include "globals.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIdirectory.hh"

#include "DEMETRARunAction.hh"
#include "DEMETRARunActionMessenger.hh" 

//---------------------------------------------------------------------------//

DEMETRARunActionMessenger::DEMETRARunActionMessenger(DEMETRARunAction *runAct):fRunAction(runAct)
{
  fDirectory = new G4UIdirectory("/DEMETRA/");
  
  fOutFileCmd = new G4UIcmdWithAString("/DEMETRA/outfile",this);
  fOutFileCmd->SetGuidance("Set the output file name without extension");
  fOutFileCmd->SetGuidance("(default: out)");
  fOutFileCmd->SetParameterName("choice",false);

  fOutFileCutCmd = new G4UIcmdWithAnInteger("/DEMETRA/cutoutfile",this);
  fOutFileCutCmd->SetGuidance("  Choice : 0 1 2 3");
  fOutFileCutCmd->SetGuidance("If 0 saves everything; if 1 saves only events with energy in crystal+scintillator; if 2 saves just events with energy in crystal");
  fOutFileCutCmd->SetParameterName("cutoutfile",true);
  fOutFileCutCmd->SetDefaultValue(1);

  fRegisterOnCmd = new G4UIcmdWithAnInteger("/DEMETRA/registeron",this);
  fRegisterOnCmd->SetGuidance("  Choice : 0 1");
  fRegisterOnCmd->SetGuidance("If 1 saves flux, neutron and isotope data; if 0 does not save these data");
  fRegisterOnCmd->SetParameterName("registeron",true);
  fRegisterOnCmd->SetDefaultValue(0);

  fTotTCmd = new G4UIcmdWithAnInteger("/DEMETRA/tottree",this);
  fTotTCmd->SetGuidance("  Choice : 0 1");
  fTotTCmd->SetGuidance("If 0 the tree containing the generation information for each event is not saved");
  fTotTCmd->SetParameterName("tottree",true);
  fTotTCmd->SetDefaultValue(1);
    
  fOutFileSaveHitCmd = new G4UIcmdWithAnInteger("/DEMETRA/save_hits_branches",this);
  fOutFileSaveHitCmd->SetGuidance("Choice : 0 1");
  fOutFileSaveHitCmd->SetGuidance("If 0 the tree branches containing the hits information for each event are not saved");
  fOutFileSaveHitCmd->SetParameterName("save_hits_branches",true);
  fOutFileSaveHitCmd->SetDefaultValue(0);
  
}

DEMETRARunActionMessenger::~DEMETRARunActionMessenger()
{
  delete fOutFileCmd;
  delete fOutFileCutCmd;
  delete fRegisterOnCmd;
  delete fTotTCmd;
  delete fOutFileSaveHitCmd;
}

//---------------------------------------------------------------------------//

void DEMETRARunActionMessenger::SetNewValue(G4UIcommand *command,
					      G4String newValue)
{
  if (command == fOutFileCmd ) 
    {
      fRunAction->SetOutFile(newValue);
    }
  else if (command == fOutFileCutCmd )
    {
      G4int cutoutfile = fOutFileCutCmd->GetNewIntValue(newValue);
      if(cutoutfile>=0 && cutoutfile<=3)
	{
	  fRunAction->SetOutFileCut(cutoutfile);
	}
      else
	{
	  G4cout << "WARNING! The setting for /DEMETRA/cutoutfile is not in the range allowed. Using the default cut (/DEMETRA/cutoutfile 1)" << G4endl;
	}
      //fRunAction->SetOutFileCut(fOutFileCutCmd->GetNewIntValue(newValue));
    }
  else if (command == fRegisterOnCmd )
    {
      fRunAction->SetRegisterOn(fRegisterOnCmd->GetNewIntValue(newValue));
    }
  else if (command == fTotTCmd )
    {
      fRunAction->SetTotT(fTotTCmd->GetNewIntValue(newValue));
    }
  else if (command == fOutFileSaveHitCmd )
    {
        fRunAction->SetHitsInfo(fOutFileSaveHitCmd->GetNewIntValue(newValue));
    }
}
