//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: DEMETRAActionInitialization.cc 68058 2013-03-13 14:47:43Z gcosmo $
//
/// \file DEMETRAActionInitialization.cc
/// \brief Implementation of the DEMETRAActionInitialization class

#include "DEMETRAActionInitialization.hh"
//#include "G4VUserPrimaryGeneratorAction.hh"
#include "DEMETRAPrimaryGeneratorAction.hh"
#include "DEMETRARunAction.hh"
#include "DEMETRAEventAction.hh"
#include "DEMETRASteppingAction.hh"
//#include "DEMETRAStackingAction.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DEMETRAActionInitialization::DEMETRAActionInitialization(DEMETRADetectorConstruction* det)
  : G4VUserActionInitialization(),fDetector(det)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DEMETRAActionInitialization::~DEMETRAActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DEMETRAActionInitialization::BuildForMaster() const
{
  SetUserAction(new DEMETRARunAction(fDetector));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DEMETRAActionInitialization::Build() const
{
  DEMETRAPrimaryGeneratorAction* gen_action = new DEMETRAPrimaryGeneratorAction(fDetector);
  //G4VUserPrimaryGeneratorAction* gen_action = new DEMETRAPrimaryGeneratorAction(fDetector);
  SetUserAction(gen_action);

  DEMETRARunAction* run_action = new DEMETRARunAction(fDetector);
  SetUserAction(run_action);
  
  DEMETRAEventAction* event_action = new DEMETRAEventAction(run_action,fDetector);
  SetUserAction(event_action);
  
  G4UserSteppingAction* stepping_action = new DEMETRASteppingAction(fDetector,event_action);
  SetUserAction(stepping_action);

  //  SetUserAction(new DEMETRAStackingAction);
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
