#include "globals.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIdirectory.hh"
#include "G4PhysicalVolumeStore.hh"

#include "DEMETRAEventAction.hh"
#include "DEMETRAEventActionMessenger.hh" 

//---------------------------------------------------------------------------//

DEMETRAEventActionMessenger::DEMETRAEventActionMessenger
(DEMETRAEventAction *evact):fEvAct(evact)
{
  fDirectory = new G4UIdirectory("/DEMETRA/");
  
  fRepFreqCmd = new G4UIcmdWithAnInteger("/DEMETRA/reportingfrequency",this);
  fRepFreqCmd->SetGuidance("Set the reporting frequency");
  fRepFreqCmd->SetGuidance("(default: 1000)");
  fRepFreqCmd->SetParameterName("choice",false);
  fRepFreqCmd->SetRange("choice>0");
}

DEMETRAEventActionMessenger::~DEMETRAEventActionMessenger()
{
  delete fRepFreqCmd;
}

//---------------------------------------------------------------------------//

void DEMETRAEventActionMessenger::SetNewValue(G4UIcommand *command,
					      G4String newValue)
{
  if (command == fRepFreqCmd ) 
    {
      fEvAct->SetRepFreq(fRepFreqCmd->GetNewIntValue(newValue));
    }
}
