#include "DEMETRADetectorMaterial.hh"

#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

DEMETRADetectorMaterial* DEMETRADetectorMaterial::fDEMETRADetectorMaterial = NULL;

DEMETRADetectorMaterial::DEMETRADetectorMaterial()
{

    //**********************************************************************
    //   DEFINITION OF MATERIALS
    //**********************************************************************

    man = G4NistManager::Instance();
    G4double a;  // atomic mass
    G4double z;  // atomic number
    G4double density;
    G4String name, symbol;
    G4int ncomponents, natoms;
    G4double fractionmass;
    
    Air = new G4Material("AirGas", z= 7., a= 14.01*g/mole, density= 1.2*kg/m3);
    
    G4Element* elC  = man->FindOrBuildElement("C");
    G4Element* elH = man->FindOrBuildElement("H");
    G4Element* elO = man->FindOrBuildElement("O");
    G4Element* elK = man->FindOrBuildElement("K");
    
    G4Element* elI = man->FindOrBuildElement("I");
    G4Element* elNa = man->FindOrBuildElement("Na");
    //G4Element* elN = man->FindOrBuildElement("N");
    G4Element* elAl = man->FindOrBuildElement("Al");
    G4Element* elMn = man->FindOrBuildElement("Mn");
    G4Element* elSi = man->FindOrBuildElement("Si");
    G4Element* elCa = man->FindOrBuildElement("Ca");
    
    O = man->FindOrBuildMaterial("G4_O");
    Na = man->FindOrBuildMaterial("G4_Na");
    K = man->FindOrBuildMaterial("G4_K");
    B = man->FindOrBuildMaterial("G4_B");
    Al = man->FindOrBuildMaterial("G4_Al");
    Cu = man->FindOrBuildMaterial("G4_Cu");
    Fe = man->FindOrBuildMaterial("G4_Fe");
    Pb = man->FindOrBuildMaterial("G4_Pb");
    Co = man->FindOrBuildMaterial("G4_Co");
    Ni = man->FindOrBuildMaterial("G4_Ni");
    Si = man->FindOrBuildMaterial("G4_Si");
    In = man->FindOrBuildMaterial("G4_In");
    
    Teflon  = man->FindOrBuildMaterial("G4_TEFLON");
    
    //PyrexGlass  = man->FindOrBuildMaterial("G4_Pyrex_Glass");
    //Modified Pyrex glass for Vessel PMTs
    //fraction masses for Pyrex Glass taken from Geant manual 
    //http://geant4.web.cern.ch/geant4/workAreaUserDocKA/Backup/Docbook_UsersGuides_beta/ForApplicationDeveloper/html/apas08.html
    PyrexGlass = new G4Material("PyrexGlass", density = 0.3389*g/cm3 , ncomponents = 6);
    PyrexGlass->AddMaterial (B, fractionmass = 0.0400639);
    PyrexGlass->AddMaterial (O, fractionmass = 0.539561);
    PyrexGlass->AddMaterial (Na, fractionmass = 0.0281909);
    PyrexGlass->AddMaterial (Al, fractionmass = 0.011644);
    PyrexGlass->AddMaterial (Si, fractionmass = 0.377219);
    PyrexGlass->AddMaterial (K, fractionmass = 0.00332099);


    NaI = new G4Material("NaI", density = 3.67*g/cm3 , ncomponents = 2);
    NaI->AddElement(elNa, natoms=1);
    NaI->AddElement(elI, natoms=1);
    
    PC_Scint = new G4Material(name="Scintillator", density = 0.88*g/cm3, ncomponents=2);
    PC_Scint->AddElement(elC, natoms=9);
    PC_Scint->AddElement(elH, natoms=12);
    
    Quartz = new G4Material ("Quartz", 2.200 * g/cm3, ncomponents = 2);
    Quartz->AddElement (elSi, natoms = 1);
    Quartz->AddElement (elO, natoms = 2);
    
    Ceramic = new G4Material ("Ceramic", 3.7 * g/cm3, ncomponents = 2);
    Ceramic->AddElement (elAl, natoms = 2);
    Ceramic->AddElement (elO, natoms = 3);
    
    Kovar = new G4Material ("Kovar", 8.8 * g/cm3, ncomponents = 3);
    Kovar->AddMaterial (Ni, fractionmass = 0.29);
    Kovar->AddMaterial (Co, fractionmass = 0.18);
    Kovar->AddMaterial (Fe, fractionmass = 0.53);
    
    //lngs rock material definition
    density = 2.71*g/cm3;
    lngsRock = new G4Material(name="LNGSRock",density,ncomponents=7);
    lngsRock->AddElement(elC,fractionmass=11.88*perCent);
    lngsRock->AddElement(elO,fractionmass=48.92*perCent);
    lngsRock->AddElement(elMn,fractionmass=5.58*perCent);
    lngsRock->AddElement(elAl,fractionmass=1.03*perCent);
    lngsRock->AddElement(elSi,fractionmass=1.27*perCent);
    lngsRock->AddElement(elK,fractionmass=1.03*perCent);
    lngsRock->AddElement(elCa,fractionmass=30.29*perCent);
    
    //---------
    
    G4double massOfMole = 1.008*g/mole;
    G4double temperature = 2.73*kelvin;
    G4double pressure = 3.e-18*pascal;
    
    Vacuum = new G4Material("Vacuum",1.,massOfMole, density= 1.e-25*g/cm3,kStateGas,temperature, pressure);
    Water  = man->FindOrBuildMaterial("G4_WATER");
    Steel  = man->FindOrBuildMaterial("G4_STAINLESS-STEEL");
    PE  = man->FindOrBuildMaterial("G4_POLYETHYLENE");
    Concrete  = man->FindOrBuildMaterial("G4_CONCRETE");
    
    
    //**********************************************************************
    //   DEFINITION OF VISUALIZATION ATTRIBUTES
    //**********************************************************************
    PEVis = new G4VisAttributes(G4Color(0.8,0.83,0.8)); //Polyethilene
    WaterVis = new G4VisAttributes(G4Color(0.08,0.4,1)); //Water
    AirVis = new G4VisAttributes(G4Color(1.,1.,0.4)); //Air
    VacuumVis = new G4VisAttributes(G4Color(1.,1.,0.4));
    CopperVis = new G4VisAttributes(G4Colour(1.,0.,0.));
    CopperVis->SetForceWireframe(true);

}

DEMETRADetectorMaterial::~DEMETRADetectorMaterial()
{
}

DEMETRADetectorMaterial* DEMETRADetectorMaterial::GetInstance()
{
  if (fDEMETRADetectorMaterial == NULL) {
    fDEMETRADetectorMaterial = new DEMETRADetectorMaterial();
  }
  return fDEMETRADetectorMaterial;
}

G4Material* DEMETRADetectorMaterial::Material(G4String what)
{
  G4Material* material = 0;
  if(what == "Vacuum")            material = Vacuum;
  if(what == "Air")               material = Air;
  if(what == "O")                 material = O;
  if(what == "Na")                material = Na;
  if(what == "K")                 material = K;
  if(what == "B")                 material = B;
  if(what == "Al")                material = Al;
  if(what == "Cu")                material = Cu;
  if(what == "Fe")                material = Fe;
  if(what == "Pb")                material = Pb;
  if(what == "Co")                material = Co;
  if(what == "Ni")                material = Ni;
  if(what == "Si")                material = Si;
  if(what == "In")                material = In;
  if(what == "Teflon")            material = Teflon;
  if(what == "PyrexGlass")        material = PyrexGlass;
  if(what == "NaI")               material = NaI;
  if(what == "PC_Scint")          material = PC_Scint;
  if(what == "Quartz")            material = Quartz;
  if(what == "Ceramic")           material = Ceramic;
  if(what == "Kovar")             material = Kovar;
  if(what == "lngsRock")          material = lngsRock;
  if(what == "Water")             material = Water;
  if(what == "Steel")             material = Steel;
  if(what == "PE")                material = PE;
  if(what == "Concrete")          material = Concrete;

  return material;
}

G4VisAttributes* DEMETRADetectorMaterial::VisAttributes(G4String what)
{
  G4VisAttributes* vis = 0;
  if(what == "PE")            vis = PEVis;
  if(what == "Water")         vis = WaterVis;
  if(what == "Air")           vis = AirVis;
  if(what == "Vacuum")        vis = VacuumVis;
  if(what == "Copper")        vis = CopperVis;
  return vis;
}
				  
G4Material* DEMETRADetectorMaterial::FindOrBuildMaterial(const G4String& name, G4bool isotopes, G4bool warning)
{
  return man->FindOrBuildMaterial(name, isotopes, warning);  
}
