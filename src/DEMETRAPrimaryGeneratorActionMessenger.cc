#include "DEMETRAPrimaryGeneratorActionMessenger.hh"
#include "DEMETRAPrimaryGeneratorAction.hh"
#include "G4UIcmdWithAString.hh"

#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DEMETRAPrimaryGeneratorActionMessenger::DEMETRAPrimaryGeneratorActionMessenger(DEMETRAPrimaryGeneratorAction* pGen)
  :G4UImessenger(),fDEMETRAPrimaryGeneratorAction(pGen)
{ 
  fDEMETRAGenTypeCmd = new G4UIcmdWithAString("/DEMETRA/gentype",this);
  fDEMETRAGenTypeCmd->SetGuidance("Set the generator type. Possible choices: GPS, AtmosphericMuons");
  fDEMETRAGenTypeCmd->SetCandidates("GPS AtmosphericMuons");
  fDEMETRAGenTypeCmd->SetParameterName("choice",false);
  fDEMETRAGenTypeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DEMETRAPrimaryGeneratorActionMessenger::~DEMETRAPrimaryGeneratorActionMessenger()
{
  delete fDEMETRAGenTypeCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DEMETRAPrimaryGeneratorActionMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{ 
  if (command == fDEMETRAGenTypeCmd)
    { fDEMETRAPrimaryGeneratorAction->SetGenType(newValue);}
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
