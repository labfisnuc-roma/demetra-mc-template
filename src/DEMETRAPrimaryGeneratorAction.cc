/* ************************************************
 * INFN RM1 GEANT4 SIMULATION GROUP 
 *
 * File:      PrimaryGeneratorAction.cc
 *
 **************************************************/

#include "DEMETRAPrimaryGeneratorAction.hh"
#include "DEMETRAPrimaryGeneratorActionMessenger.hh"
#include "DEMETRAAtmosphericMuonGenerator.hh"
#include "DEMETRADetectorConstruction.hh"
#include "Randomize.hh"
#include "globals.hh"
#include <math.h>
#include "G4ParticleGun.hh"
#include "G4GeneralParticleSource.hh"
#include "G4ThreeVector.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4TransportationManager.hh"
#include "G4RunManager.hh"
#include "G4Gamma.hh"
#include "G4Event.hh"
#include "G4GeneralParticleSource.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#define PI 3.14159265
using namespace std;

DEMETRAPrimaryGeneratorAction::DEMETRAPrimaryGeneratorAction(DEMETRADetectorConstruction* myDC):myDetector(myDC),fGenType("GPS"),fMessenger(0)
{
  n_particle = 1;
  fMessenger = new DEMETRAPrimaryGeneratorActionMessenger(this);
  particleGunGPS = new G4GeneralParticleSource();
  particleGunAtmMu = new DEMETRAAtmosphericMuonGenerator(myDC);  

  // default particle
  
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* particle = particleTable->FindParticle("e-");
  particleGunGPS->SetParticleDefinition(particle);
  particle_energy = 1.0*MeV;  
}

DEMETRAPrimaryGeneratorAction::~DEMETRAPrimaryGeneratorAction()
{
  delete fMessenger;
  delete particleGunGPS;
  delete particleGunAtmMu;
}

void DEMETRAPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{ 

  if (fGenType == "GPS") {
    //std::cout << "Selected particle generator GPS" << std::endl;
    G4int numParticles=1;
    particleGunGPS->SetNumberOfParticles(numParticles);
    particleGunGPS->GeneratePrimaryVertex(anEvent);
  }
  else if (fGenType == "AtmosphericMuons") {
    //std::cout << "Selected particle generator AtmMu" << std::endl;
    particleGunAtmMu->GeneratePrimaries(anEvent);
  }
}

void DEMETRAPrimaryGeneratorAction::SetGenType(G4String gentype) {fGenType = gentype;}
