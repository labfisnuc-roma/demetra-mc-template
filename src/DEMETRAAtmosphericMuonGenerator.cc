/* ************************************************
 * INFN RM1 GEANT4 SIMULATION GROUP 
 *
 * File:      DEMETRAAtmosphericMuonGenerator.cc
 *
 **************************************************/

#include "DEMETRAAtmosphericMuonGenerator.hh"
#include "DEMETRADetectorConstruction.hh"
#include "Randomize.hh"
#include <CLHEP/Random/RandGeneral.h>
#include "globals.hh"
#include <math.h>
#include "G4ParticleGun.hh"
#include "G4ThreeVector.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4TransportationManager.hh"
#include "G4ParticleMomentum.hh"
#include "G4RunManager.hh"
#include "G4Gamma.hh"
#include "G4Event.hh"
#include "G4GeneralParticleSource.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#define PI 3.14159265
using namespace std;

DEMETRAAtmosphericMuonGenerator::DEMETRAAtmosphericMuonGenerator(DEMETRADetectorConstruction* myDC):myDetector(myDC)
{
  n_particle = 1;
  particleGun = new G4ParticleGun();
  //fMessenger = new DEMETRAPrimaryGeneratorActionMessenger(this);

  // default particle
  
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* particle = particleTable->FindParticle("mu-");
  particleGun->SetParticleDefinition(particle);
  particle_energy = 1.0*MeV;  
}

DEMETRAAtmosphericMuonGenerator::~DEMETRAAtmosphericMuonGenerator()
{
  //delete fMessenger;
  delete particleGun;
}

void DEMETRAAtmosphericMuonGenerator::GeneratePrimaries(G4Event* anEvent)
{ 
  G4int numParticles=1;
  particleGun->SetNumberOfParticles(numParticles);

  G4double ene = GenerateMuonEnergy();

  particleGun->SetParticleEnergy(ene*GeV);

  G4double theta = GenerateMuonDirection();
  particleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));

  particleGun->GeneratePrimaryVertex(anEvent);
}

G4double DEMETRAAtmosphericMuonGenerator::GenerateMuonDirection()
{ 
  G4double ThetaMuon = 0.;
  std::vector<double> dNdTheta(nBinTheta);

  for(unsigned i = 0; i < nBinTheta; ++i){
    ThetaMuon = i*Thetastep;
    dNdTheta[i] = NormTheta*pow(cos(ThetaMuon),exp_n);
  }
    
  CLHEP::RandGeneral randGeneral(dNdTheta.data(), nBinE);
  G4double random = randGeneral.shoot();

  cout << "Theta: " << random << endl;
  return random;
}

G4double DEMETRAAtmosphericMuonGenerator::GenerateMuonEnergy()
{ 

  G4double Emuon = 0.;
  std::vector<double> dNdE(nBinE);

  for(unsigned i = 0; i < nBinE; ++i){
    Emuon = (i+0.5)*Estep; // steps of 1 GeV
    //    cout << "Emuon: " << Emuon << endl;
    dNdE[i] = Izero*NormE*pow((Ezero+Emuon),-exp_n)*pow((1.+Emuon/epsilon),-1.);
    //    cout << "dNdE " << dNdE[i] << endl;
  }
  
  CLHEP::RandGeneral randGeneral(dNdE.data(), nBinE);
  G4double random = randGeneral.shoot();
  G4double ene_random = EminMu + random*(EmaxMu-EminMu);

  cout << "Energy: " << ene_random << endl;
  return ene_random;

}

